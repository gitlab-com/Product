# GitLab Product Department Repository
* [Handbook](https://about.gitlab.com/handbook/product/)

## GitLab Product Process

This repository also includes code to automate the scheduled creation of issues for the Product team.

## Auto-generated Issues Usage

Add a new item to [issues.yml](https://gitlab.com/gitlab-com/Product/-/blob/main/config/issues.yml) and [sections.yml (optional)](https://gitlab.com/gitlab-com/Product/-/blob/main/config/sections.yml)


| Name | Values | Description |
| ----------- | ----------- | ----------- |
| name | `String` | Inserted into the title of the generated issue. |
| scope | `all`, `section` | Determines at what level the issue will be created.  |
| template | `String` | Inserted as the description of the generated issue. Will parse template in `/templates` (for `section` scoped) or `/.gitlab/issue_templates` (for `all` scoped) directory and render as markdown |
| day_of_week (optional) | `Sun`, `Mon`, `Tue`, `Wed`, `Thu`, `Fri`, `Sat` | If present, will run weekly on the given value. |
| weekly (optional) | `odd`, `even` | If present, will scope the weekly run to only happen based on the given value |
| day_of_month (optional) | `Int` | If present, automation will run on the day of the month of the given value. |
| day_of_year (optional) | `Array` | If present, automation will run on the day of the year of the given value(s). |
| quarterly (optional) | `first_day_of_quarter`, `last_day_of_quarter`, `first_day_of_last_month_of_quarter` | If present, automation will run on the given value. |
| frequency (optional) | `String` | If present, overrides the default frequency (based on `day_of_*`) that is added to the issue title. |
| include_calendar_week_year (optional) | `Boolean` | If present, adds the calendar year based on calendar week to the issue title based on `frequency` |

### Day of Week

```yml
- name: 'Weekly Priority Setting - Kenny'
  day_of_week: Sun
  scope: all
  template: Weekly-Priority-Setting-Kenny.md
```

### Day of Month

```yml
- name: 'Ops Section Direction Updates'
  day_of_month: 18
  scope: all
  template: Ops-Section-Direction-Updates.md
```

### Day of Year (override "Yearly" with "Biannually")

```yml
- name: 'Product Milestone Creation'
  day_of_year: [30, 210]
  scope: all
  template: 'Create-Milestones.md'
  frequency: 'Biannually'
```

## Testing Locally
To test the create_issues script use the `--dry-run` arguement. Before running, make sure you set the `CI_PROJECT_ID` (for the product project, it's 1641463) and the `GITLAB_API_PRIVATE_TOKEN` environment variables.

```
bundle exec ruby create_issues.rb --dry-run
```

## Testing YAML Syntax

YAML can be hard. You can check the YAML file syntax locally with these commands:

First install YAMLlint
```
gem install yamllint
```

And once installed testing the two primary yaml files
```
yamllint config/issues.yml config/sections.yml
```

## Limitations

* The only scopes that are available are `section` and `all`. We should consider adding `group` that would allow for the creation of an issue per product group. 
* In order to use multiple quick actions you have to add an extra line space between them

# ProdOps Slackbot

This project also contains configuration for sending messages through the ProdOps Slackbot.

`prod_ops_slackbot.rb` is a Slackbot which sends reminders to a Slack channel based on the schedule defined in `config/slackbot_messages.yml`. The Slackbot is created using the Slack-Ruby-Client library and communicates with Slack API to post messages. The script uses ENV['PRODOPS_SLACKBOT_API_PRIVATE_TOKEN'] to authenticate to the Slack API.

## `prod_ops_slackbot.rb`

### Classes

#### Slackbot

This class is responsible for the main functionality of the Slackbot. The class initializes a Slack API client and loads the messages from the config/slackbot_messages.yml file. The start method of the class sends the messages defined in the YAML file if they match the schedule defined in the YAML file.

**Methods**

- `#start` - sends the messages to the Slack channel defined in the YAML file.

**Private Methods**

- `#get_messages_to_send` - selects the messages to send based on the schedule defined in the YAML file.
- `#schedule_match?(message)` - checks if the schedule of the message matches the current date.
- `#send_messages` - sends the messages to the Slack channel defined in the YAML file.

## `slackbot_messages.yml`

Add a new reminder with the following fields:

```yaml
`name`: the name of the reminder, used to identify it in the configuration file
`channel`: the name of the Slack channel to send the reminder to, do not include the "#" in the channel name
`schedule`: the schedule for when to send the reminder, contains the following fields:
  `type`: the type of schedule, can be one of weekly, monthly, or annual
  `utc_hour_of_day`: the hour of the day to send the reminder on, only needed if type is weekly
  `day_of_week`: the day of the week to send the reminder on, only needed if type is weekly
  `day_of_month`: the day of the month to send the reminder on, only needed if type is monthly or annual
  `month`: the month to send the reminder in, only needed if type is annual
`blocks_file`: the filename of a yml file in `templates/slack_blocks` that contains the message to be sent
```

Each reminder has a header block that contains a plain text header with the reminder message, and a section block that contains a markdown message with further details about the reminder.

## Removing Slackbot Sent Messages

- Obtain the message link in Slack, e.g. `https://gitlab.slack.com/archives/C0NFPSFA8/p1675446821790259`
- Extract the last string, `p1675446821790259`, and modify it to `1675446821.790259` by removing the "p" and adding a "." after the 10th number.
- This value is the `ts` identifier.
- Set up a local Slack @client using the following code:

```rb
Slack.configure do |config|
  config.token = ENV['PRODOPS_SLACKBOT_API_PRIVATE_TOKEN']
end

@client = Slack::Web::Client.new
```
- Delete the message with `@client.chat_delete(channel: '#product', ts: '1675446821.790259', as_user: true)`
- Replace `'#product'` with the channel the message was posted in and `ts` with the value you determined above
