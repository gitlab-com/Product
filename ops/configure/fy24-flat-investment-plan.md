# FY24 Configure flat investment plan

What progress does the Configure stage expect to achieve with the current level of investment in FY24?

Note: The following page may contain information related to upcoming products, features and functionality. It is important to note that the information presented is for informational purposes only, so please do not rely on the information for purchasing or planning purposes. Just like with all projects, the items mentioned on the page are subject to change or delay, and the development, release, and timing of any products, features or functionality remain at the sole discretion of GitLab Inc. 

## Context

Right now, [the Configure team](https://about.gitlab.com/handbook/engineering/development/ops/configure/) is fully staffed with 8 engineers (7 BE, 1 FE), TW, EM, PD, PM and UX manager. The team owns [5 product categories](https://about.gitlab.com/handbook/product/categories/#configure-group) (ChatOps was recently demoted). We only plan feature development on the [Kubernetes Management](https://about.gitlab.com/direction/configure/kubernetes_management/) and [Deployment Management](https://about.gitlab.com/direction/configure/deployment_management/) categories. The other categories are in maintenance mode, where we are only doing bugfixes, maintenance and supporting community contributions.

In FY23, our focus was slowly shifting from [Kubernetes management](https://about.gitlab.com/direction/configure/kubernetes_management/) to [deployment management](https://about.gitlab.com/direction/configure/deployment_management/). Deployment management is a main pillar of our [Delivery direction](https://about.gitlab.com/direction/delivery/).

As a hot topic, we are discussing switching our GitOps engine from a custom built, `agentk`-based solution to Flux. This plan assumes that we will switch to Flux.

## Play for FY24

We would 

- mature [the GitOps offering to Viable](https://gitlab.com/gitlab-org/configure/general/-/issues/321),
- [remove the certificate-based integration](https://gitlab.com/gitlab-org/configure/general/-/issues/199) in %17.0 and
- ship the core elements of [the deployment management solution](https://about.gitlab.com/direction/configure/deployment_management/#the-deployment-management-project).

### Priorities

1. GitOps support to [Viable maturity](https://gitlab.com/gitlab-org/configure/general/-/issues/321)
1. Integrate a [Kubernetes overview](https://gitlab.com/gitlab-org/gitlab/-/issues/375449) in the [Environment pages](https://gitlab.com/gitlab-org/gitlab/-/issues/352186)
1. [Support for clusters that can't see GitLab](https://gitlab.com/gitlab-org/gitlab/-/issues/350943)
1. Offer an alternative to [GitLab Managed cluster](https://gitlab.com/groups/gitlab-org/-/epics/8874)
1. Start work on and ship [the GitLab deployment project](https://about.gitlab.com/direction/configure/deployment_management/#the-deployment-management-project)
1. Ship a [Kubernetes dashboard](https://gitlab.com/groups/gitlab-org/-/epics/2493) built-into GitLab
1. Automate access to manifests, [containers](https://gitlab.com/gitlab-org/gitlab/-/issues/363113) and helm charts stored in GitLab


| Topic | January | February | March | April | May | June | July | August | September | October | November | December | January |
|-------|---------|----------|-------|-------|-----|------|------|--------|-----------|---------|----------|----------|---------|
| [Rework the GitOps documentation to be based around Flux](https://gitlab.com/groups/gitlab-org/configure/-/epics/12) | X | X | | | | | | | | | | | |
| [GitOps support to Viable](https://gitlab.com/gitlab-org/configure/general/-/issues/321) | X | X | X | X | | | | | | | | | |
| [Integrate a Kubernetes overview in the Environment pages](https://gitlab.com/gitlab-org/gitlab/-/issues/375449) | X | X | X | | | | | | | | | | |
| [Support for clusters that can't see GitLab](https://gitlab.com/gitlab-org/gitlab/-/issues/350943) | | | X | X | X | | | | | | | | |
| [Offer an alternative to GitLab Managed cluster](https://gitlab.com/groups/gitlab-org/-/epics/8874) | | | X | X |x | | | | | | | | |
| [Start work on and ship the GitLab deployment project](https://about.gitlab.com/direction/configure/deployment_management/#the-deployment-management-project) | | | | | X | X | X | X | X | X | X | X | X |
| [Ship a Kubernetes dashboard](https://gitlab.com/groups/gitlab-org/-/epics/2493) | | | X | X | X | X | X | X | X | | | | |
| [Automate access to manifests, containers and helm charts stored in GitLab](https://gitlab.com/gitlab-org/gitlab/-/issues/363113) | X | X | X | X | | | | | | | | | |

### Product metric goals

- Adoption of the agent on SaaS
- Adoption of the agent on Self-Managed
- MAU of GitOps usage
- MAU of CI/CD sharing
- MAU of Kubernetes dashboard usage
- Effect on Environment page MAU
- Adoption of the GitLab deployment pipeline on SaaS
- Adoption of the GitLab deployment pipeline on Self-Managed

| Topic | Starting | Q1 | Q2 | Q3 | Q4 |
|---------|---------|-----|-----|-------|---------|
| [CI/CD MAU](https://app.periscopedata.com/app/gitlab/656814/Kubernetes-integration?widget=15256566&udv=1012098) | 8,000* | 16,000 | 22,000 | 26,000 | 30,000 |
| [Number of active agents (SaaS)](https://dashboards.gitlab.net/d/kas-main/kas-overview?orgId=1&viewPanel=28) | 7,400** | 15,000 | 25,000 | 35,000 | 43,000 |
| [Number of active agents (SM)](https://app.periscopedata.com/app/gitlab/656814/Kubernetes-integration?widget=16032187&udv=1012098) | 11,800** | 21,000 | 36,000 | 50,000 | 67,000 |
| [Percentage of SM instances with an agent (Premium)](https://app.periscopedata.com/app/gitlab/656814/Kubernetes-integration?widget=9639658&udv=1012098) | 11%** | 13% | 15% | 17% | 20% |
| [GitOps MAU](https://gitlab.com/gitlab-org/gitlab/-/issues/366294) | | | X | ? | ? |

* \* - estimated value
* ** - November value

## Further considerations

- https://gitlab.com/gitlab-com/Product/-/issues/3832
