# FY24 Configure -10% investment thought experiment

What impact would a -10% investment in FY24 have on the Configure direction? What impact would losing 1.5 headcount have on the Configure direction?

Note: The following page may contain information related to upcoming products, features and functionality. It is important to note that the information presented is for informational purposes only, so please do not rely on the information for purchasing or planning purposes. Just like with all projects, the items mentioned on the page are subject to change or delay, and the development, release, and timing of any products, features or functionality remain at the sole discretion of GitLab Inc. 

## Context

Right now, [the Configure team](https://about.gitlab.com/handbook/engineering/development/ops/configure/) is fully staffed with 8 engineers (8 BE, 1 FE), TW, EM, PD, PM and UX manager. I'll assume that we would lose the investment from engineering.

In FY23, our focus was slowly shifting from [Kubernetes management](https://about.gitlab.com/direction/configure/kubernetes_management/) to [deployment management](https://about.gitlab.com/direction/configure/deployment_management/). Deployment management is a main pillar of our [Delivery direction](https://about.gitlab.com/direction/delivery/).

## Proposal

The primary consideration is the trade-off between stopping/delaying the work planned on Deployment management or slowing down the work on the Kubernetes integrations.

### Planned progress without lost investment

See the [flat investment plan](./fy24-flat-investment-plan.md)

### The effect of the lost investment

Migrating towards Flux and improving our GitOps offering would remain our top priority. The lost investment would result in a likely slowdown in the Deployment management direction and would jeopardize our plans to remove the certificate-based integration in 17.0.

### Effect on product metrics

- Adoption of the agent on SaaS
- Adoption of the agent on Self-Managed
- MAU of GitOps usage
- MAU of CI/CD sharing
- MAU of Kubernetes dashboard usage
- Effect on Environment page MAU
- Adoption of the GitLab deployment pipeline on SaaS
- Adoption of the GitLab deployment pipeline on Self-Managed

| Topic | Starting | Q1 change/reference | Q2 | Q3 | Q4 |
|---------|---------|-----|-----|-------|---------|
| [CI/CD MAU](https://app.periscopedata.com/app/gitlab/656814/Kubernetes-integration?widget=15256566&udv=1012098) | 8000* | 0/10,000 | 0/22,000 | -1K/26,000 | -2K/30,000 |
| [Number of active agents (SaaS)](https://dashboards.gitlab.net/d/kas-main/kas-overview?orgId=1&viewPanel=28) | 7,400** | 0/15,000 | 0/25,000 | -2K/35,000 | -4K/43,000 |
| [Number of active agents (SM)](https://app.periscopedata.com/app/gitlab/656814/Kubernetes-integration?widget=16032187&udv=1012098) | 11,800** | 0/21,000 | 0/36,000 | -2K/50,000 | -4K/67,000 |
| [Percentage of SM instances with an agent (Premium)](https://app.periscopedata.com/app/gitlab/656814/Kubernetes-integration?widget=9639658&udv=1012098) | 0/11%** | 0/13% | -1%p/15% | -1%p%17% | -2%p/20% |
| [GitOps MAU](https://gitlab.com/gitlab-org/gitlab/-/issues/366294) | |  | X | ? | ? |
