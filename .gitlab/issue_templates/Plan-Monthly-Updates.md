# Introduction

Product Managers own and update various deliverables that communicate direction to the rest of the company on a recurring basis. This issue tracks updates to those items!

## Tasks

### Product Direction

* [ ] @gweaver - Plan:Project Management - Category visions reviewed and Direction pages updated
* [ ] @amandarueda - Plan:Product Planning - Category visions reviewed and Direction pages updated
* [ ] @hsnir1 - Plan:Optimize - Category visions reviewed and Direction pages updated
* [ ] @mmacfarlane -  Plan:Knowledge- Category visions reviewed and Direction pages updated
* [ ] @gweaver - Plan - Category visions reviewed and Direction pages updated

### OKRs

* [ ] @gweaver - Plan:Project Management - OKR planning and updates
* [ ] @amandarueda - Plan:Product Planning -  OKR planning and updates
* [ ] @hsnir1 - Plan:Optimize -  OKR planning and updates
* [ ] @mmacfarlane -  Plan:Knowledge- OKR planning and updates
* [ ] @gweaver - Plan - OKR planning and updates

### Product Roadmap 

* [ ] @gweaver - Plan:Project Management 
    * [ ] [Roadmap slides](https://docs.google.com/presentation/d/1-QUDo9j3NHZC2hOrv0P5tRyWouTdM2QFFToYunZ0Bv4/edit#slide=id.g1c7bf510d85_0_4137)  
    * [ ] Review Roadmap changes with EM and gain [sign-off](https://gitlab.com/gitlab-org/plan-stage/plan-engineering/-/wikis/Plan-Roadmap-Signoffs)  
* [ ] @amandarueda - Plan:Product Planning 
    * [ ] [Roadmap slides](https://docs.google.com/presentation/d/1-QUDo9j3NHZC2hOrv0P5tRyWouTdM2QFFToYunZ0Bv4/edit#slide=id.g1c7bf510d85_0_4137)  
    * [ ] Review Roadmap changes with EM and gain [sign-off](https://gitlab.com/gitlab-org/plan-stage/plan-engineering/-/wikis/Plan-Roadmap-Signoffs) 
* [ ] @hsnir1 - Plan:Optimize
    * [ ] [Roadmap slides](https://docs.google.com/presentation/d/1-QUDo9j3NHZC2hOrv0P5tRyWouTdM2QFFToYunZ0Bv4/edit#slide=id.g1c7bf510d85_0_4137)  
    * [ ] Review Roadmap changes with EM and gain [sign-off](https://gitlab.com/gitlab-org/plan-stage/plan-engineering/-/wikis/Plan-Roadmap-Signoffs) 
* [ ] @mmacfarlane -  Plan:Knowledge
    * [ ] [Roadmap slides](https://docs.google.com/presentation/d/1-QUDo9j3NHZC2hOrv0P5tRyWouTdM2QFFToYunZ0Bv4/edit#slide=id.g1c7bf510d85_0_4137)  
    * [ ] Review Roadmap changes with EM and gain [sign-off](https://gitlab.com/gitlab-org/plan-stage/plan-engineering/-/wikis/Plan-Roadmap-Signoffs) 
* [ ] @gweaver - Plan 
    * [ ] [Roadmap slides](https://docs.google.com/presentation/d/1-QUDo9j3NHZC2hOrv0P5tRyWouTdM2QFFToYunZ0Bv4/edit#slide=id.g1c7bf510d85_0_4137)  
    * [ ] Review Roadmap changes with EM and gain [sign-off](https://gitlab.com/gitlab-org/plan-stage/plan-engineering/-/wikis/Plan-Roadmap-Signoffs) 


Make changes to this template [here](https://gitlab.com/gitlab-com/Product/-/blob/main/.gitlab/issue_templates/Plan-Monthly-Updates.md)

/assign @hsnir1, @gweaver, @amandarueda, @mmacfarlane
/due in 15 days

