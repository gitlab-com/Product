# Introduction

Product Managers own and update various deliverables that communicate direction to the rest of the company on a recurring basis. This issue tracks updates to those items!

## Tasks

### Product Direction - Category visions reviewed and Direction pages updated

* [ ] AST::Static Analysis - @connorgilbert
* [ ] AST::Compostion Analysis - @johncrowley 
* [ ] AST::Dynamic Analysis - @johncrowley 
* [ ] AST::Secret Detection - @abellucci 
* [ ] AST stage - @sarahwaldner
* [ ] SRM::Security Insights - @dagron1
* [ ] SRM::Security Policies - @g.hickman 
* [ ] SRM::Security Platform Management - @smeadzinger 
* [ ] SRM stage - @dagron1
* [ ] SSCS::Authentication - @hsutor
* [ ] SSCS::Authorization - @jrandazzo 
* [ ] SSCS::Pipeline Security - @jocelynjane
* [ ] SSCS::Compliance - @khornergit 
* [ ] SSCS Stage - @sam.white 

### Group Priorities pages
* [ ] AST::Static Analysis - @connorgilbert
* [ ] AST::Compostion Analysis - @johncrowley 
* [ ] AST::Dynamic Analysis - @johncrowley 
* [ ] AST::Secret Detection - @abellucci 
* [ ] AST stage - @sarahwaldner
* [ ] SRM::Security Insights - @dagron1
* [ ] SRM::Security Policies - @g.hickman 
* [ ] SRM::Security Platform Management - @smeadzinger 
* [ ] SRM stage - @dagron1
* [ ] SSCS::Authentication - @hsutor
* [ ] SSCS::Authorization - @jrandazzo 
* [ ] SSCS::Pipeline Security - @jocelynjane
* [ ] SSCS::Compliance - @khornergit 
* [ ] SSCS Stage - @sam.white 

### [Product Roadmap Slides](https://docs.google.com/presentation/d/1-QUDo9j3NHZC2hOrv0P5tRyWouTdM2QFFToYunZ0Bv4/edit#slide=id.g2f5aef72b14_0_3123) - Roadmap slides updated

* [ ] AST::Static Analysis - @connorgilbert
* [ ] AST::Compostion Analysis - @johncrowley 
* [ ] AST::Dynamic Analysis - @johncrowley 
* [ ] AST::Secret Detection - @abellucci
* [ ] SRM::Security Insights - @dagron1
* [ ] SRM::Security Policies - @g.hickman 
* [ ] SRM::Security Platform Management - @smeadzinger 
* [ ] SSCS::Authentication - @hsutor
* [ ] SSCS::Authorization - @jrandazzo 
* [ ] SSCS::Pipeline Security - @jocelynjane
* [ ] SSCS::Compliance - @khornergit

### [DevSecOps Catalog Slides](https://docs.google.com/presentation/d/1XcOmwUvaSEYhj87dvHj05l3t-JuBEcLaFdhSEDvX4EY/edit#slide=id.g2633147b2e9_3_5277) - Catalog slides updated

* [ ] AST::Static Analysis - @connorgilbert
* [ ] AST::Compostion Analysis - @johncrowley 
* [ ] AST::Dynamic Analysis - @johncrowley 
* [ ] AST::Secret Detection - @abellucci
* [ ] AST stage - @sarahwaldner
* [ ] SRM::Security Insights - @dagron1
* [ ] SRM::Security Policies - @g.hickman 
* [ ] SRM::Security Platform Management - @smeadzinger 
* [ ] SRM stage - @dagron1
* [ ] SSCS::Authentication - @hsutor
* [ ] SSCS::Authorization - @jrandazzo 
* [ ] SSCS::Pipeline Security - @jocelynjane
* [ ] SSCS::Compliance - @khornergit 
* [ ] SSCS Stage - @sam.white


Make changes to this template [here](https://gitlab.com/gitlab-com/Product/-/blob/main/.gitlab/issue_templates/Sec_Section_Monthly_Updates.md)

/assign @connorgilbert, @johncrowley, @abellucci, @sarahwaldner, @dagron1, @hsutor, @jrandazzo, @jocelynjane, @khornergit, @sam.white, @smeadzinger
/due in 15 days