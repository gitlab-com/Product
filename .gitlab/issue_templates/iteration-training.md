## Iteration Training

[Iteration](https://about.gitlab.com/handbook/values/#iteration) is one of six [GitLab Values](https://about.gitlab.com/handbook/values).  This template is intended to be an onboarding introduction to new team members, and a refresher for those who have been with GitLab.

### Steps

- [ ] Assign this issue to yourself with the title of Iteration Training - First Name Last Name - FY##Q#

#### Review
  
- [ ] [Minimal Valuable Change (MVC)](https://about.gitlab.com/handbook/product/product-principles/#the-minimal-valuable-change-mvc)
- [ ] [Iteration](https://about.gitlab.com/handbook/product/product-principles/#iteration)
- [ ] [Move Fast](https://about.gitlab.com/handbook/values/#move-fast-by-shipping-the-minimal-valuable-change)
- [ ] [Product Processes: Iteration Strategies](https://about.gitlab.com/handbook/product/product-processes/#iteration-strategies)
- [ ] [Cross-Culture Collaboration Guide](https://about.gitlab.com/company/culture/cross-culture-collaboration-guide/). It is important to be aware that while iteration is a company value at GitLab and at the core of how we operate, iteration does not come easily for all people. By understanding how different cultures and groups approach iteration and the learning curve associated with adapting to an iterative work culture, we can collaborate from a more empathetic lens. While the content in the guide does not explicitly focus on iteration, generating awareness around things like `Collaboration Focus` (task-oriented or relationship-oriented) directly relates to the speed at which people ship. 

#### Watch
  
- [ ] [Interview about iteration in engineering with Christopher and Sid](https://www.youtube.com/watch?v=tPTweQlBS54)
- [ ] [Iteration Office Hours with CEO](https://www.youtube.com/watch?v=liI2RKqh-KA)
- [ ] [Youtube playlist with collected iteration content](https://www.youtube.com/playlist?list=PL05JrBw4t0KqJOf21O9cbAMxtpUpdLFoZ) _Optional_

#### Actions

- [ ] If you are a manager, hold an iteration retrospective with your development group. (Consider leaving a comment in this issue with the link to the document or async issue)
  - [ ] Discuss past examples of successful iteration.  Why was it successful?  How did it meet the definition of an MVC?
  - [ ] Discuss past examples that could use improvement.  How did the example not meet the definition of an MVC? In what ways could you have iterated differently? List specific examples.
  - [ ] Using the results of these discussions, choose a piece of work that's ready for development and discuss how it could be better iterated on.
  - [ ] Identify areas of improvement for the team and incorporate into your [Product Development Workflow](https://about.gitlab.com/handbook/product-development-flow/). Consider to schedule a review on the results in a few months later.
  - [ ] Update your teams handbook page with ideas and improvements you've incorporated to improve iteration
  - [ ] Update this training template to iterate and improve on our iteration training _Optional_
- [ ] If you are a manager, consider encouraging your team members to complete the iteration training

#### Questions to ask yourself

- During planning - [is the task the smallest thing possible?](https://about.gitlab.com/handbook/values/#move-fast-by-shipping-the-minimal-valuable-change)  If not, cut the scope.
- At the end of the milestone - did something slip because it was too big?
  - Be sure to note this in the team retro and how you would have approached this differently through the lens of iteration
  - If this happens regularly, schedule an iteration retro

#### Self assess

- [ ] We use [Google Docs](https://about.gitlab.com/handbook/communication/#google-docs) as a communication standard. Review your recent documents and proposals.  How many of them could have [started with a MR](https://about.gitlab.com/handbook/communication/#start-with-a-merge-request)?
- [ ] Consider scheduling a task to review your MRs at the end of each milestone.  How many could have been broken down into smaller iterations?
- [ ] (Optional) Add a comment to this issue linking to an issue/MR where you proposed breaking down the feature into smaller iterations.
