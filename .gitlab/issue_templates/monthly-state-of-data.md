# Month Year State of Product Analytics Data

This issue summarizes the overall health and macro trends of our product usage data. The intent is for Product Managers, Product Analysis, and other consumers of Product Usage Data to have a better sense of these broader trends before analyzing individual results. 

## :book: References

- [Product Adoption Dashboard](https://app.periscopedata.com/app/gitlab/771580/Product-Adoption-Dashboard)
- [Service Ping Health Dashboard](#)

## :notepad_spiral: Summary

## Recommendations and Guidelines

## Systems Health

### Underlying data systems 
Summary of any issues, defects, or data quality problems with our underlying product analytics systems for this month. 

### ⚙ Service Ping 

#### Self Managed

#### SaaS

### ❄ Snowplow

### 💾 Postgres Replica

### 📚 Data Models

## 📈 Trends and Macro Analysis


/assign @justinfarris @amandarueda @mlaanen @dpeterson1

/label ~"State of Data" 
