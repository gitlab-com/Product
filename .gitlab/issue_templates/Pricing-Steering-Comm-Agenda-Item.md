## Request Type

- [ ] New SKU
- [ ] Update an existing SKU `provide rationale for the change request`
    - **IMPORTANT**: If the price of an existing SKU is changed, renewal quotes that have not been sent to Zuora will be renewed at the current product catalog list price due to our [Zuora Billing Settings configuration](https://knowledgecenter.zuora.com/Zuora_Billing/Billing_and_Invoicing/Billing_Settings/Define_Default_Subscription_and_Order_Settings#:~:text=Use%20Latest%20Product%20Catalog%20Pricing%20uses%20the%20price%20for%20the%20product%20that%20is%20in%C2%A0the%20product%20catalog%20when%20a%20subscription%20is%20renewed.). If a different renewal price was agreed upon, the quote will need to be manually adjusted to that price prior to sending the renewal quote to Zuora.

## Submitter
_It is the Submitters responsibility to ensure all required information and appropriate approvals are obtained for each Step prior to progressing forward in the SKU process. Please read the handbook page on [How to Create New or Update a SKU](https://about.gitlab.com/handbook/business-technology/enterprise-applications/guides/product-catalog/#how-to-create-new-or-update-a-sku)._

- [ ] Have you referenced if the SKU is already available in the Product Catalog to ensure it does not already exist?
- [ ] Please assign the SKU Request Issue to yourself and complete all sections of Step 1 before proceeding to Step 2.

## 1. Product Information

1. Overview of Product/Service: `short description of the value provided to the Customer`
1. Rate Plan Name: `display name on quotes and in the product`
1. Rate Plan Charge Description `Referenced by CDot and used for web checkout display. For sales-assited, short description of product when quoting. Please ensure to indicate a value / updated value.`
1. Desired Go-Live Date: `when is this SKU expected to be used`

## 2. Proposed Pricing/SKU Information

_Some of these fields will be filled in on [New SKU Request issue](https://about.gitlab.com/handbook/business-technology/enterprise-applications/guides/product-catalog/)_

1. Provide a link to the Cost of Goods Sold (COGS) spreadsheet (make a copy of [this template](https://docs.google.com/spreadsheets/d/1em_4RiKOzvA3W9N4FxjmDxH6Rtr4my_o6ZifSXEWz0o/edit#gid=1853638008))
    - [ ] COGS spreadsheet link
1. Provide a justification if project margins are below 55% for internally devliered services
1. How is the proposed SKU (meant to be) sold to Customers:
   - [ ] Self-serve - {+Fulfillment Approval required in Step 2 of New SKU Process+}
   - [ ] Sales-assisted (SFDC Quote)
1. Product/Service Type for Quoting:
   - [ ] Base Product (Ex.  GitLab tier)
   - [ ] Add On Product (Ex.  Storage, Professional Service)
1. Charge Type:
   - [ ] Recurring:  continually billed until removed from a Subscription
   - [ ] One-Time:  only billed once
   - [ ] Usage ***:  billed in arrears based on consumption
      - Any Included Units? `example:  phone plan with 1000 included minutes with overage fees after`
1. Unit of Measure (UOM):
   - [ ] Seats: for licenses
   - [ ] N/A (UOM cannot be configured for Flat Fee pricing)
   - [ ] Other: `proposed UOM`
1. Charge Model:
   - [ ] Per Unit Pricing:  product/service is priced per UOM
      - List Price: $`price` per UOM per year
   - [ ] Flat Fee: product/service is a single fixed price
      - List Price: $`price` per year
   - [ ] Tiered ***:  product/service is progressively priced as volume changes
      - List Price: `please describe how pricing changes per tier`
   - [ ] Volume ***:  product/service is priced based on the volume purchased
      - List Price: `please describe how price changes depending on quantity purchased`
1. Charge Timing: How is the Customer expected to pay?
   - [ ] Upfront for an annual term
   - [ ] Upfront for a multi-year term ***
   - [ ] Annually + in advance of service for a multi-year term ***
   - [ ] Semi-Annually + in advance of service for at least a 1-year term ***
   - [ ] Quarterly + in advance of service for at least a 1-year term ***
   - [ ] Monthly + in advance of service for at least a 1-year term ***
   - [ ] In arrears ***
1. List Price:
   - $`price` per UOM per `year` (time period if not one-time/usage, likely 1-year)


## TODO: add cc with `@` for committee members
## TODO: add label quick actions
