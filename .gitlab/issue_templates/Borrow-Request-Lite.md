<!-- Use this issue for tracking team members who help out with other group work. This isn't a formal Borrow Request where multiple team members need to work on something else for a specific period of time, or multiple projects change in priority. Use this when team members pick up an issue or two in another group, or works on something else for a few milestones because they have extra bandwidth.-->

## Borrow Request Overview

<!-- Outline the goals and reasoning supporting the Borrow Request, including rough time frame and scope of effort (how many days, weeks, or milestones -->

### Current Staffing and Priorities

<!-- Using a table, outline current staffing and priorities being worked on -->

<!-- Example
| Team | BE | FE | EM | SDET | Product Design | Current Priorities |

 <!-- Highlight the teams affected and number of team members for each - a table is helpful to illustrate
| Team | BE IC |  FE IC | EM | PM | UX | SET | Current Priorities |
|------|-------|--------|----|----|----|-----|------|
| Team 1 |  3  |  4     | 1  | 1  | 1  | 0   | Database partioning |
| Team 2 |  4  |  4     | 1  | 1  | 0  | 0   | Database sharding |
-->

### Exit Criteria

<!-- Define the exit criteria expected from the Borrow Request -->

### Proposal

<!-- Define the ask:
  1. Who is the team member
  2. What work are they picking up, and for how long
  3. Is there a trade-off that occurred
  4. Define DRI
-->

<!-- Example
1. **Fulfillment: Provision Product Designer will pick up two issues for Release in 14.9. This is because they have capacity to do so due to a backlog of blocked work.
1. Timeline: 25% of bandwidth in 14.9
1. The DRI is the Product Design Manager for Fulfillment
-->


### Issue Links

<!-- Link to relevant issue/s -->

### Onboarding Tasks

<!-- List tasks for team members to get acquainted with the project and/or team -->


## Process Overview

- [ ] Create issue outlining work change and reasoning
- [ ] Share issue with impacted Group Product Managers (including Growth PM if the borrows may come from their team)
- [ ] Engineering Managers+
- [ ] Product Design Managers
- [ ] Complete any scoping of the work to be done
- [ ] Request review and approval from your manager
- [ ] Identify any onboarding tasks needed for success
- [ ] Incorporate feedback into the issue description
- [ ] Execute
- [ ] Retrospective - Create an issue (optional), or use an existing Retrospective issue (for example, if a Product Designer works with another Section for one milestone, they could share feedback on the experience in the UX Retrospective.)

/confidential
/label ~"borrow::requested" 
