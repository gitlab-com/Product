## Monthly Competitive Roadmap Review

1. Review competitor roadmaps
1. Share insights this issue
1. Follow up with individual groups/categories

## References
*  [ ] GitHub: https://github.com/orgs/github/projects/4247/views/1
  * Changelog is available by clicking on `Menu` on the right-hand side
* [ ] Atlassian/BitBucket: https://www.atlassian.com/roadmap/cloud?selectedProduct=bitbucket
  * [ ] JiraAlign: https://www.atlassian.com/roadmap/cloud?selectedProduct=jiraAlign
  * [ ] Jira SW: https://www.atlassian.com/roadmap/cloud?selectedProduct=jsw
  * [ ] Confluence: https://www.atlassian.com/software/confluence
* [ ] CircleCI: https://circleci.com/product-roadmap/
* [ ] Gerrit: https://www.gerritcodereview.com/roadmap.html
* [ ] JetBrains: https://www.jetbrains.com/space/roadmap/
* [ ] Xray (Jira): https://docs.getxray.app/display/XRAY/Roadmap
* [ ] Azure DevOps: https://docs.microsoft.com/en-us/azure/devops/release-notes/features-timeline
* [ ] PlanView
* [ ] Deployment:
  * [ ] Waypoint: https://www.waypointproject.io/docs/roadmap 
  * [ ] Harness: https://developer.harness.io/release-notes/whats-new/
  * [ ] Argo CD: https://argo-cd.readthedocs.io/en/stable/roadmap/
  * [ ] Octopus Deploy: https://octopus.com/company/roadmap
* [ ]  Monitor:
  * [ ] Datadog
  * [ ] GrafanaLabs
* [Previous Issues](https://gitlab.com/gitlab-com/Product/-/issues?scope=all&utf8=%E2%9C%93&state=closed&label_name[]=Competitive%20Roadmap%20Review)

## Opening
* [ ] Create Retrospective Thread - @jreporter
* [ ] Set the issue due date for mid-month - @jreporter

## Tasks 

### Issue Creation 

1. Identify any issues we also have in the backlog that are also on the competitor roadmaps by adding a `competitive` label 
1. Create new issues that are gaps in our backlog or features that would be a differentiator for GitLab and add the `competitive` label 
   1. For GitHub specific issues, add `gh-competitive` 

### Section reviewers
Review for implications to your sections groups and direction. Provide a highlight comment and ping your team

* [ ] Dev: @mushakov
* [ ] CI/CD: @jreporter
* [ ] Sec: @sarahwalder
* [ ] SaaS/Core Platforms: @fzimmer 

### Group reviewers

* [ ] Global Search: @bvenker
* [ ] Dedicated: @cbalane
    * https://github.com/orgs/github/projects/4247/views/1?filterQuery=label%3A%22github+ae%22

## Closing
* [ ] Add Summary of competitor review 
* [ ] Make [updates to this template](https://gitlab.com/gitlab-com/Product/-/edit/main/.gitlab/issue_templates/Competitive-Roadmap-Review.md) based on retrospective thread - @dhershkovitch

/assign @jreporter @bvenker @fzimmer @cbalane @sarahwalder @mushakov @steve-evangelista 
/cc @gl-product-pm

/label ~"Competitive Roadmap Review"
/due in 14 days
