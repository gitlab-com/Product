# Fulfillment Monthly Async OKR Review

Each month the ~"section::fulfillment" PMs & EMs review and provide status updates on the current quarter's OKRs. Updates are then reviewed by product, engineering and UX leadership.

As of February 2025, this process is fully asynchronous.

## :book: References

* [OLD - Meeting Notes](https://docs.google.com/document/d/17smuC22Ncu5PP0Ao9QdZnkWK0nbe7ArJveDRp-95AGE/edit)
* [Past Prep Issues](https://gitlab.com/gitlab-com/Product/-/issues?scope=all&utf8=%E2%9C%93&state=all&label_name[]=PI%20Review%20Prep&label_name[]=section%3A%3Afulfillment)
* [Fulfillment current quarter OKRs - list view](https://gitlab.com/gitlab-com/gitlab-OKRs/-/issues/?sort=milestone_due_desc&state=opened&label_name%5B%5D=FY26-Q1&label_name%5B%5D=devops%3A%3Afulfillment&first_page_size=100)
* [Fulfillment current quarter OKRs - top level objective](https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/10382)

## :white_check_mark: Tasks

### :o: Opening Tasks
* [ ] Set a due date for this issue 2 weeks from creation to allow for the team to provide async updates for OKRs - @courtmeddaugh
 
### :heavy_check_mark: Other Tasks
* [ ] Check remaining follow ups from [last review](https://gitlab.com/gitlab-com/Product/-/issues?scope=all&utf8=%E2%9C%93&state=all&label_name[]=PI%20Review%20Prep&label_name[]=section%3A%3Afulfillment) - @courtmeddaugh

### :star: OKR updates

Each group should update their **OKRs in GitLab**, with a focus on ~"priority::1" and ~"priority::2" OKRs first. Make sure to update:
* Progress
* Health status
* Add a comment explaining the progress and health status

#### Group OKR Updates

* [ ] Provision - @ppalanikumar 
* [ ] Provision Eng KRs - @rhardarson 
* [ ] Subscription Management - @tgolubeva
* [ ] Subscription Management Eng KRs - @dzubova 
* [ ] Utilization - @courtmeddaugh (interim) / @jameslopez
* [ ] Utilization Eng KRs - @jameslopez
* [ ] Fulfillment Platform - @ppalanikumar 
* [ ] Fulfillment Platform Eng KRs - @jameslopez

### :mag: Manager Reviews

* [ ] Review and ask questions on this issue or within individual OKRs: 
   * [ ] OKRs with ~UX label - @esybrant
   * [ ] Dev Error Budgets - @jeromezng
   * [ ] Triage and Quality Eng Items - @jameslopez 
   * [ ] Test Platform - @kkolpakova
   * [ ] OKRs - @courtmeddaugh

### :x: Closing Tasks
* [ ] Make [adjustments to the template](https://gitlab.com/gitlab-com/Product/-/edit/main/.gitlab/issue_templates/Fulfillment-Section-PI-Review.md) based on the retrospective thread - `@courtmeddaugh`
* [ ] Add a highlights comment and share on this issue - `@courtmeddaugh`

/assign @courtmeddaugh @tgolubeva @dzubova @rhardarson @jameslopez @jeromezng @esybrant @kkolpakova @ppalanikumar

/label ~"section::fulfillment"  ~"OKR Review Prep"
