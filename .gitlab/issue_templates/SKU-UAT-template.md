# SKU User Acceptance Testing (UAT) Template
## Summary

This issue will be used to prepare for and conduct user acceptance testing  for the new GitLab SKU:
`[ENTER SKU NAME HERE]`.
<br>For specific UAT instructions please review this [handbook page](https://handbook.gitlab.com/handbook/business-technology/enterprise-applications/guides/product-catalog/#how-to-request-the-creation-or-modification-of-a-sku).

## UAT Type

- [ ] New SKU
- [ ] Update to an existing SKU

## Pre-Testing Checklist
### Stakeholder Alignment
- [ ] Set a timeline for the test session and to inform all the DRIs about the upcoming testing session. Confirm that this testing timline works for the DRIs.
- [ ] Communicate with Testing teams to identify a DRI to participate in the testing session.
- [ ] Communicate and get approval on expected time window for the testing session from all DRIs.

### Test Case Readiness
- [ ] Create a copy of [the master spreadsheet](https://docs.google.com/spreadsheets/d/1awATKGhVKLRh3LpyVllCnCQYsV4vjakoijsW5K8dY_4/edit?gid=2069598564#gid=20695985640). Specific instructions for creating test cases can be found in the master spreadsheet or at this [handbook page](https://handbook.gitlab.com/handbook/business-technology/enterprise-applications/guides/product-catalog/).
- [ ] Add new test cases related to the new SKU or SKU change
- [ ] Ensure that all Testing DRIs review and approve all test cases prior to conducting testing.

### System Readiness
Enterprise Applications team mark completion of the systems readiness items below prior to testing.
- [ ] Deploy Zuora functionality to Staging
- [ ] Deploy SFDC functionality to Staging (including approval matrix update)
- [ ] Generate test data in SFDC

## Testing Timeline
Several key dates and timing need to be established and approved by all Testing DRIs and `@caroline.swanson` from Enterprise Applications.

- Date when Ent Apps will complete their SKU build in staging/test environment
- Date when UAT test cases should be finalized and approved by all Testing DRIs
- Date when Ent Apps will have test data generated
- Date when Cross-functional Testing DRIs will conduct their testing
  - 2-3 biz days: Sales Ops / Deal Desk
  - 2-3 biz days: Billing & AR
  - 2-3 biz days: Fulfillment, Data, Revenue, FP&A
- Date when User acceptance final sign off/approval

## Testing DRIs
The below list should represent the DRI for each relevant team who will be assisting in UAT. Confirm with the people identified below that:
1. They are the correct Testing DRI 
2. Add in their testing dates and confirm the Testing DRI is available on those dates.
3. Confirm the Testing DRI has reviewed and approved the test cases

- [ ] **Sales Ops**: `@jrabbits` -- _Checking this box indicates confirmation of the DRI identified_
  - [ ] **Testing Availability** -- _Checking this additional box confirms availability during `TESTING DATES GO HERE` testing window_
  - [ ] **Test Case Approval** -- _Checking this additional box indicates that you have reviewed test scenarios and approved the proposed cases._

- [ ] **Billing**: `@JosephineHararah`  -- _Checking this box indicates confirmation of the DRI identified_
  - [ ] **Testing Availability** -- _Checking this additional box confirms availability during `TESTING DATES GO HERE` testing window_
  - [ ] **Test Case Approval** -- _Checking this additional box indicates that you have reviewed test scenarios and approved the proposed cases._

- [ ] **Revenue**: `@msubramanian @ankurbedi` -- _Checking this box indicates confirmation of the DRI identified_
  * [ ] **Testing Availability** -- _Checking this additional box confirms availability during `TESTING DATES GO HERE` testing window_
  * [ ] **Test Case Approval** -- _Checking this additional box indicates that you have reviewed test scenarios and approved the proposed cases._

- [ ] **Data**: `@iweeks @annapiaseczna @rakhireddy` -- _Checking this box indicates confirmation of the DRI identified_
  - [ ] **Testing Availability** -- _Checking this additional box confirms availability during `TESTING DATES GO HERE` testing window_
  - [ ] **Test Case Approval** -- _Checking this additional box indicates that you have reviewed test scenarios and approved the proposed cases._

- [ ] **FP&A Insights and Analytic:** `@vagrawalg` -- _Checking this box indicates confirmation of the DRI identified_
  - [ ] **Testing Availability** -- _Checking this additional box confirms availability during `TESTING DATES GO HERE` testing window_
  - [ ] **Test Case Approval** -- _Checking this additional box indicates that you have reviewed test scenarios and approved the proposed cases._

- [ ] **Fulfillment:** `@courtmeddaugh @ppalanikumar @tgolubeva` -- _Checking this box indicates confirmation of the DRI identified_
  - [ ] **Testing Availability** -- _Checking this additional box confirms availability during `TESTING DATES GO HERE` testing window_
  - [ ] **Test Case Approval** -- _Checking this additional box indicates that you have reviewed test scenarios and approved the proposed cases._

## Test Scenarios
  `Add the link to the test scenarios here`

## Bugs Identified
| Test Case # | Bug Summary | Raised by | Issue Link | DRI | Status |
|-------------|-------------|-----------|------------|-----|--------|

## Final Testing Sign Off
The identified testing DRIs must provide their sign off as a requirement to SKU deployment.
- [ ] Pricing: `@seanhall`
- [ ] Fulfillment: `@courtmeddaugh @ppalanikumar @tgolubeva`
- [ ] Sales: `@jrabbits`
- [ ] Billing: `@JosephineHararah`
- [ ] Revenue: `@ankurbedi @msubramanian`
- [ ] Central Data Team DRI: `@iweeks @annapiaseczna @rakhireddy`
- [ ] FP&A Insights and Analytics DRI: `@vagrawalg`