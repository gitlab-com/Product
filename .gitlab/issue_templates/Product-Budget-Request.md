<!-- This template is for use in request new items for budget unrelated to headcount or growth and development expenses. Examples of this would include contractor spend, infrastructure cost changes, and research vendors -->

### Opening Tasks 

- [ ] Update Title for FYXX-QX - Product Budget Request for [Replace with  Budget Item]
- [ ] Assign the issue to yourself, the VP of Product, and your manager 
- [ ] Add a comment for a Retrospective Thread 
- [ ] Fill in the template sections below

### Budget Request 

- [ ] New Budget Item (new request for R&D Budget)
- [ ] Updating an Existing Budget (update an existing budget line item)

#### Budget Request Overview & Justification

1. Budget Item Description: [Add 1-2 sentence description of the budget item]
1. Type of Budget Item: [Vendor, Infrastructure, Tooling, Other]
1. Expected Timeline for Expense: [Add expected Timeline for when the budget item would be needed]
1. Budget Item Justification: [Brief explanation on why this budget item is needed]

### Approvals 

- [ ] Manager 
- [ ] VP of Product 

### Next Steps 

- [ ] If approved by manager and VP of Product, the VP product will add this to the finance business partner planning for forecast if an adjustment to an existing item or budget if to be addressed in a future quarter 
- [ ] If declined, close issue and resolve any Retrospective Thread Items 
