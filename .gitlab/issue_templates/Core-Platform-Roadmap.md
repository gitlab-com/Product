## Core Platform Monthly Roadmap Updates

The Core Platform section maintains our roadmap slides here: https://docs.google.com/presentation/d/1vWxvSTGeArXlBFAjCaSkOWPHhR3GMEKPK6h2IeTqAN4/edit#slide=id.g2c568ff1307_0_2306. These slides are linked into the main Product roadmap deck [here](https://docs.google.com/presentation/d/1-QUDo9j3NHZC2hOrv0P5tRyWouTdM2QFFToYunZ0Bv4/edit?pli=1#slide=id.g262aee42aa7_11_57), so that they automatically stay in sync.

These decks are used both internally and externally, and needs to be kept up to date monthly.

### Instructions

Review the slide deck for any changes since last months update. This includes addition of new items, changes to the projected delivery, feature details, or the removal of no longer planned items.

Relevant section, group and feature/theme slides should be reviewed:

* Section level slides
* Group level slides where applicable
* Feature/theme highlight slides where applicable

#### Updates

- [ ] ~"section::core platform" @joshlambert
- [ ] ~"group::cloud connector" @sguyon
- [ ] ~"group::database" @sranasinghe
- [ ] ~"group::distribution" @dorrino
- [ ] ~"group::geo" @sranasinghe
- [ ] ~"group::global search" @bvenker
- [ ] ~"group::gitaly" @mjwood
- [ ] ~"group::tenant scale" @lohrc
- [ ] ~"group::import and integrate" @m_frankiewicz
- [ ] ~"group::personal productivity" @jtucker_gl
- [ ] ~"group::design system" @jtucker_gl

cc @Cdoyle203