## Borrow Request Overview

<!-- Outline the goals and reasoning supporting the Borrow Request -->

### Exit Criteria

<!-- Define the exit criteria expected from the Borrow Request -->

### Proposal

<!-- Define the ask:
  1. Which team will receive engineers and/or designers
  2. Estimated length of engagement
  3. Propose teams to source from
  4. Define DRI
-->

<!-- Example
1. **3 Backend Engineers** need to be added to ________ group in order to meet reliability goal ABC.
1. **1 Product Designer** needs to be added to ________ group in order to meet reliability goal ABC.
1. Timeline: 3 milestones ending in milestone 14.5.
1. The Borrow Request DRI for development will be the EM of ________ group.
1. The Borrow Request DRI for Product Design will be the Product Design Manager of ________ group.
-->

### Requirements

<!-- Define any specific requirements that are important, such as certain skillsets that are required -->

### Projects

<!-- Using a table, outline the project and proposed team members -->

<!-- Example
| Priorities | Project | Team Members |
|------------|---------|----------|
| Top Priority for ~"Engineering Allocation" | [Deliver Infradev Issues](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&state=opened&label_name[]=infradev&label_name[]=devops%3A%3Averify)| `List of Engineers here`
-->

### Current Staffing

<!-- Using a table, outline current staffing -->

<!-- Example
| Team | BE | FE | EM | SDET | Product Design
|------|-------|-----|----|------|------|
| Pipeline Execution | (4) `Engineer1` `Engineer2` | 0 | 1 | 0 | (1) `Designer1` |
| Pipeline Authoring | (1) `Engineer3` | 0 | 0 | 0 | 0 | 0 |
| Testing | (1) `Engineer4` | 0 | 0 | 0 | 0 | 0 |
| Runner | 2 | 0 | 0 | 0 | 0 | 0 |
| **Total** | **8** | **0** | **0** | **0** | **0** | **1** |
-->

### Staffing Request

<!-- Using a table, outline the request from priority teams -->

<!-- Example
| Team | BE | FE | EM | SDET | Product Design
|------|-------|-----|------|-----|-----|
| Monitor | 2 | 0 |0 | 0 | 0 |
| Configure | 1 | 0 |0 | 0 | 1 |
| **Total** | **3** | **0** | **0** | **0** | **0** | **1** |
-->

## Process Overview

A Borrow Request consists of 3 distinct phases:

1. Proposal
1. Rollout
1. Retrospective

### Proposal Phase
This template contains a checklist summarizing tasks to be completed. The goal of this checklist is greater Transparency, Efficiency and achieving strong Results.

Scope of transparency: Product Manager + Engineering Manager + Section Leadership + R&D Leadership

#### Proposal Scope Check (Product Owned)
- [ ] Define business need
- [ ] Create and link to initial scope
- [ ] Ping Section Leadership (Development, Quality, Design, Product Management) for review to ensure a Borrow is the appropriate mechanism for accomplishing the goal before proceeding. 

### Approvals
- [ ] Development Section Leaders
- [ ] Quality Section Leaders
- [ ] Design Section Leaders
- [ ] Product Management Section Leaders

If a borrow spans multiple sections, the following approvals are also required
- [ ] CPO `@david`
- [ ] CTO's direct reports with developrs if the borows may include team members on their teams `@bmarnane` `@timzallmann` `@meks`
- [ ] If the borrow request includes anyone from the (Growth section)[https://about.gitlab.com/handbook/product/categories/#growth-section] then approval from `@s_awezec` is required

#### If Not Approved to Proceed (Product Owned)
- [ ] Update the [Borrow handbook page content](https://about.gitlab.com/handbook/product/product-processes/#borrow) to ensure any new criteria for when to use a Borrow request is included.

#### If Approved to Proceed (Development & UX Owned)
- [ ] Decide on the estimated timeline, including a tentative transition date.
- [ ] If the transition date is < 30 days out, document why.
- [ ] Define the number of team members needed and with what skills (Frontend, Backend, Product Designers, Golang, Ruby, Python, specialized domain expertise, etc.)
- [ ] Clearly define exit criteria for the Borrow Request

#### Determine Borrowing Options (Development & UX Owned)
- [ ] Work with Section or Department leadership to determine eligible groups and team members
- [ ] Draft impacts of each eligible team member joining the borrow
- [ ] If there are software architecture changes that are being done in conjunction with the borrow, confirm with Development and Infrastucture that those changes are vetted and approved.
- [ ] Confirm that the planned PTO of those being borrowed is being taken into account
- [ ] Product, Development, and UX leadership designate team members who will participate in the borrow

### Rollout Phase
Once the impacted teams are decided upon, it is critical that impacted Engineering Managers and Product Design Managers are notified as soon as possible to ensure they have the correct context and understand the goals and expected timeline/rollout plan for the Borrow Request.

Rolling out the Borrow Request requires clear communication and context sharing as well as the ability to dynamically move with urgency while keeping everyone organized. Incoming ICs need to be properly onboarded to their new role. This includes fully understanding the goals, context, scope, and timeline of the Borrow Request.

Scope of transparency: Product, Development, and UX Leadership + EMs (including Product Design Managers) + ICs

Tasks:

Communicating the changes:

- [ ] Confirm receiving groups have been notified
- [ ] Confirm that the "why" of the borrow is documented from the perspective of those being borrowed
- [ ] Confirm that impacted EMs, Product Leaders and Product Design Managers have communicated directly their ICs
- [ ] Transition date, context, and project scope clearly communicated to all impacted members
- [ ] If possible, allow volunteers to come forward first to their EMs or Product Design Managers. Should no volunteers come forward, EMs select individual ICs.
- [ ] Schedule AMA with all impacted team members
- [ ] Announce the changes in relevant public Slack channels.

Onboarding Engineers:

- [ ] Onboarding buddies selected for incoming ICs.
- [ ] An onboarding issue or document should be created for the incoming IC. The issue should include a curated list of things that the IC needs to know. The receiving team should review the issue for clarity and conciseness. 
  - [ ] The exit criteria should be included and clear to the IC
- [ ] ICs should be tagged to review the specific set of issues to be accomplished during the Borrow Request.
- [ ] On the expected start date, ICs meet with onboarding buddies.
- [ ] ICs should have regular 1:1s with the receiving team's EM. 
  - [ ] The EM should articulate what team rituals and responsibilities the IC should participate in, i.e stand-ups, retrospectives, regular meetings, Slack messages. 
- [ ] PMs and/or EMs (including Product Design Managers) followup with all ICs to answer any remaining questions.
- [ ] ICs are invited to attend relevant new stand-ups and meetings.

### Retrospective Phase
Scope of transparency: Product, Development, and UX Leadership + EMs (including Product Design Managers) + ICs

Tasks:
- [ ] Retrospective issue created
- [ ] All impacted individuals (PMs, EMs, Product Design Managers, ICs etc) invited to participate
- [ ] Hold sync retrospective call
- [ ] Update this process or the [Product template](https://gitlab.com/gitlab-com/Product/-/edit/main/.gitlab/issue_templates/Borrow-Request.md) based on any retrospective findings/actions

### Relevant Documents

* [Add Document](URL)
* [Add Document](URL)
* [Add Document](URL)

/confidential
/label ~"borrow::requested"
