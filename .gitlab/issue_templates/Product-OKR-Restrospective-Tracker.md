## Context

As outlined in our OKR processes, stage leaders [should lead an OKR retrospective for their stage](/handbook/product/product-okrs/#sharing-okr-completion-at-the-end-of-the-quarter). This issue is to track completion of Stage-level OKR retrospectives. 

## Timeline

In the first two weeks of the quarter new quarter, each stage leader should conduct a retrospective a retrospective using [the Product OKR Retrospective Template](https://gitlab.com/gitlab-com/Product/-/issues/new?issuable_template=Product-OKR-Retrospective). 

Once completed, the Stage leader should mark as complete in this issue and share the retrospective results/learnings as a comment in this issue. 

- [ ] Manage
- [ ] Plan
- [ ] Create
- [ ] Verify
- [ ] Package
- [ ] Deploy
- [ ] Secure
- [ ] Monitor
- [ ] Govern
- [ ] ModelOps
- [ ] AI-powered
- [ ] Analyze
- [ ] Fulfillment
- [ ] Systems
- [ ] Data Stores
- [ ] SaaS Platforms

/label ~"Product Retrospectives" ~OKR
/due in 2 weeks
/confidential
/assign @uchetta @mushakov @derekferguson @jreporter @nagyv-gitlab @sarahwaldner @sam.white @tmccaslin @stkerr @ofernandez2 @joshlambert @fzimmer @hbenson @mflouton @justinfarris 