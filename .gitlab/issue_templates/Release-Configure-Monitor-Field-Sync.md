## 🎯 Intent

* Create a singular forum for the field team to provide input on product direction
* Share and learn between Product, GTM, and other teams

## 🔗 Link

[Notes Document](https://docs.google.com/document/d/1bq46JNOnLhIHC_wxosJ13McIkncYSW_ana7jJQ5ZGJc/edit?usp=sharing)

### 🤨 What should we discuss?

Field team members share the following:

* Customer Escalations 
* Highlight specific use cases to support
* Recent success & failure stories
* [Release Evangelism](https://about.gitlab.com/handbook/marketing/community-relations/developer-evangelism/social-media/#release-evangelism) insights (Developer Evangelism team)
* Feedback on our direction/roadmap
* Other ways Product groups can help field teams be successful
* Review top customer success / sales issues

Product team members share the following:

* Meaningful direction/roadmap updates
* Recent deliveries
* Highlight product indicators/Product metrics highlights
* Share interesting customer stories
* Ask for help on specific validation track activities

## ☑ Format

1. Collaborate asynchronously on this issue
1. Participating team members complete individual tasks
1. Sync meeting based on input in issue

### :o: Opening Tasks
* [ ] Set a due date to this issue as 1 business day prior to the scheduled sync meeting - @kbychu
* [ ] Add a retrospective thread to this issue - @kbychu
* [ ] Quick ping on owners of any follow-up items - @kbychu
* [ ] Add a comment to participants to add content and remind them of due date - @kbychu

### :heavy_check_mark: Individual Tasks
* [ ] Gather and aggregate feedback from TAMs @mbruemmer @zchua-gtlb
* [ ] Gather and aggregate feedback from SAs @jkunzmann
* [ ] Gather and aggregate feedback from Alliances, Partnerships, and GTM perspectives @DarwinJS
* [ ] Gather and aggregate feedback from marketing @anair5 @csaavedra1
* [ ] Share highlights from Configure @nagyv-gitlab
* [ ] Share highlights from Release @cbalane
* [ ] Share highlights from Monitor:Respond @abellucci
* [ ] Share highlights from Monitor:Observability @sebastienpahl

### :x: Closing Tasks
* [ ] Add a reminder to team members to capture their followups in issues and add them to the description - @kbychu
* [ ] Add video recording to issue - @kbychu
* [ ] Make [adjustments to the template](https://gitlab.com/gitlab-com/Product/-/edit/main/.gitlab/issue_templates/Release-Configure-Monitor-Field-Sync.md) based on the retrospective thread - @kbychu
* [ ] Share relevant outcomes and highlights in Slack - @kbychu

/assign @kbychu @nagyv-gitlab @cbalane @mbruemmer @jkunzmann @DarwinJS @anair5 @csaavedra1 @zchua-gtlb @sebastienpahl @abellucci


/label ~"section::ops" ~"Field Sync"
/confidential
