### Overview 

This issue is an Access Request to the Learning Lab Environment as referenced in the [Field Guide](https://gitlab.highspot.com/items/674e1346bc7ff7bced658e90#2) for GitLab Duo with Amazon Q. 

### Requestor Details 

1. First Name/ Last Name:
1. GitLab Handle: 
1. Slack Handle: 
1. Role/Title: 
1. Reason for access: 

### Provisioner tasks 

- [ ] Add user to https://demo.beta.dev.us-east-1.gitlab.falco.ai.aws.dev/
- [ ] User name: followng the format `First initiallastname`_gl under GitLab email
- [ ] Create password: Edit user under Admin settings and create PW 
- [ ] Slack username and password details to user 
- [ ] Close Issue 

/assign @jreporter 
/due in 3 days 

cc @panand3 @laurenaalves 