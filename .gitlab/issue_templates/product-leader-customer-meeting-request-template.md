# Customer Meeting Request for Product Leader Attendance 

This issue will be used to coordinate Product Leader attendance to customer calls with Product EBA @jennifergarcia20

## Filled out by Requestor

**Requestor Name** - 

**Customer Name** -

**Customer Salesforce Link**

**Please answers these questions** - 

1. What is the goal of this conversation? 
 
2. Did we set any expectations with this customer? (promises of milestones, clarification, technical assistance etc.) If so, what are they and who set them?
 
3. What are our areas of focus for the call? Which leader are you wanting to attend?

4. Is this high-level or a technical deep-dive? 

5. Is the customer expecting a demo? If so, what kind? 

6. How soon does this need to be scheduled? (ie: within 2 weeks,3 weeks, etc. )


## Tasks
- [ ] Please create an executive briefing doc by making a copy of this document: https://docs.google.com/document/d/1LZi3diy9CNdM9vZQQzlk8wzByNWxhZ6KUjz278eglZ8/edit? 
- [ ] Once all the above information and questions are answered please tag @jennifergarcia20 in the comments including a link to the completed briefing doc. TY!


/assign @jennifergarcia20