## Overview

The CPO Shadow Program is an initiative where team members of the Product division take turns shadowing the Chief Product Officer (CPO) for a specified period of time ranging from 1 to 2 weeks. The team member, in the role of a CPO shadow, will attend all customer-facing meetings with the CPO. This provides an opportunity for the CPO Shadow to gain deeper insight into customer conversations, understand how to present the company’s broader strategy and vision, and gain a better understanding of the CPO’s role in supporting existing and future customers as an [Executive Sponsor](https://handbook.gitlab.com/handbook/sales/field-operations/field-enablement/executive-sponsor-program/).

In line with our [“Results for customers”](https://handbook.gitlab.com/handbook/values/#results) value, we believe that it is very important for the Product division to be connected to our customers and better understand their pain points. The CPO shadow program puts emphasis on customer facing interactions exactly for that reason.

As the program has emphasis on customer meetings, travel is required for participation.

## Eligibility

To ensure that the plan achieves maximum impact within GitLab, all members of the Product division are eligible to apply. Please confirm with your manager before applying for the program.

## How To Apply

Update the description of this issue and complete the items below:

Provide a brief summary of your interest and eligibility for the program

` Interest details `

Explain how participating in the CPO Shadow Program aligns with your professional development goals and how it will benefit you and the organization

` Professional development goals `

Indicate your availability and flexibility to accommodate the 1-2 week shadowing period. You may complete the area below or send a DM to the EBA to the CPO.

` Availability or note you send a DM` 

Once updated, post a comment to your manager and ask them to approve your participation. Once your manager approves, post a message in the #chief-product-officer channel with a link to the issue and @mention EBA to the CPO in the message.

/confidential 