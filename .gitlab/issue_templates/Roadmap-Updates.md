## Introduction

The product team maintains [a deck](https://docs.google.com/presentation/d/1-QUDo9j3NHZC2hOrv0P5tRyWouTdM2QFFToYunZ0Bv4/edit?pli=1#slide=id.g1c7bf510d85_0_4137) that outlines the roadmap for the next year. This issue tracks updates to the content of that deck so that content is accurate.

## Tasks
 
Work with cross-functional partners to: 
* Shift dates for roadmap items 
* Add or remove items to your roadmap
* Annotate "Achievement against goal" symbols for roadmap items
* On the first month of the quarter
   * Remove the earliest column from your roadmap
   * Add a new column for the new quarter
   * Add completed items to "Recent Deliverables" slide
* When updating roadmap slides, include a link to the relevant issue / epic in the speaker notes. In that linked item include a note at the top that states the target persona, what the feature is, and the value it will deliver to that persona. This should be super short, max 2-3 sentences. Having access to that information at the top of the tickets will help the PMM team self-serve instead of having to reach out to individual PMs for this information whenever they update external roadmaps.
* Check off your name after the tasks above have been completed 

NOTE:  Some groups have group-level slides and competitor slides in addition to the general roadmap slide. Please update them all.

* [ ] Dev Create - @derekferguson
* [ ] Dev Plan - @gweaver
* [ ] Sec Secure - @sarahwaldner
* [ ] Sec Govern - @sam.white
* [ ] CI Verify - @jreporter
* [ ] CI Package @trizzi
* [ ] CD Deploy - @nagyv-gitlab
* [ ] AI-Powered - @tmccaslin
* [ ] Core Platform Manage - @joshlambert
* [ ] Core Platform Systems - @joshlambert
* [ ] Core Platform Data Stores - @joshlambert
* [ ] Monitor - @stkerr
* [ ] SaaS Platform - @fzimmer
* [ ] Fulfillment - @courtmeddaugh
* [ ] Technical Writing - @susantacker 
* [ ] Product Design - @vkarnes 
* [ ] UX Research - @karenyli

You can make changes to this template [here](https://gitlab.com/gitlab-com/Product/-/blob/main/.gitlab/issue_templates/Roadmap-Updates.md).

/assign @derekferguson,  @gweaver,  @sarahwaldner, @sam.white, @jreporter, @trizzi, @nagyv-gitlab, @tmccaslin, @joshlambert, @stkerr,  @fzimmer, @courtmeddaugh, @susantacker, @vkarnes, @karenyli, @natalie.pinto
/due in 15 days
