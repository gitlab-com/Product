# Monetization Requests - Intake Process (Lite Version)
#### Process Purpose & Overview

This intake process is meant to standardize the pricing, promotion, partnership, and other monetization requests that are received by Product, Legal, Billing, and other stakeholders.

#### Process Steps
1. Requestor submits request via this template
2. Request is recieved and reviewed by cross-functional at Pricing Committee meetings (monthly)
3. Requeset is prioritized by the Committee
4. Prioritization status is communicated to Requestor
<br> _Please note that submitting an issue does not automatically mean that the idea will be pursued._

# {+Requestor Section Start+} 
### 1. Requestor Details
- [ ] Name: `Your Name Here`
- [ ] Slack handle: `@yournamehere`
- [ ] Team: `Your team here`

<!-- blank line -->
----
<!-- blank line -->

### 2. Request Description & Business Justification
Provide a description of the request or proposal. Including: What the request is and why would this be of value to GitLab.

<!-- blank line -->
----
<!-- blank line -->
### 3. Request ROI
#### 3a. What would be the main success measure for this request if it were executed?
Provide the KPIs that would part of measuring whether this work was successful. TAM, ARR, nARR, strategic justification, etc.

#### 3b. What would be the expected revenue return if this request were executed?
`$          `

#### 3c. Are you (or your function) willing to commit to a revenue target if this request were executed?
- [ ] Yes
- [ ] No
<br> _If no, why not:_ `Provide more infomration on why not`

<!-- blank line -->
----
<!-- blank line -->
### 4. Implementation Information
#### 4a. In what quarter would this request / opportunity ideally be completed?

#### 4b. Does this request pertain to a specific customer segment? If yes - provide below
- [ ] Enterprise
- [ ] MM
- [ ] SMB
- [ ] PubSec

#### 4c. Does this request pertain to a marketplace?
- [ ] Yes // _If yes, which one?_ `provide here`
- [ ] No

#### 4d. Does this request pertain to a partnership?
- [ ] Yes // _If yes, which one?_ `provide here`
- [ ] No

<!-- blank line -->
----
<!-- blank line -->

# {+ Reviewer Section Start+} 
### 1. Reviewed by
- [ ] **Product**
<br> Reviewed by: `@taginreviewer`
<br> `Functional feedback or questions recorded here`

- [ ] **Legal**
<br> Reviewed by: `@taginreviewer`
<br> `Functional feedback or questions recorded here`

- [ ] **Revenue**
<br> Reviewed by: `@taginreviewer`
<br> `Functional feedback or questions recorded here`

- [ ] **FP&A**
<br> Reviewed by: `@taginreviewer`
<br> `Functional feedback or questions recorded here`

- [ ] **Billing**
<br> Reviewed by: `@taginreviewer`
<br> `Functional feedback or questions recorded here`

- [ ] **Other** // If other `provide function`
<br> Reviewed by: `@taginreviewer`
<br> `Functional feedback or questions recorded here`


<!-- blank line -->
----
<!-- blank line -->
# Do Not Edit Below
<!-- DO NOT EDIT BELOW -->
/confidential
<br> /label  ~"Monetization Intake::Not Started"