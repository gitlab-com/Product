## How To Request TPM Support

- [ ] **Requester**: Fill out the information below and title the issue `project name`. 
- [ ] **Requester**: Add epic "Product Technical Program Management" to this issue.
- [ ] **Approver**: Run project through TPM Calculator. Update `TPM Coverage` label to correct status. Assign to correct TPM as relevant.
- [ ] **Assigned TPM**: Once you begin work, update the description to include links to the various epics / docs for the project for future reference.

This template will automatically assign the issue to @natalie.pinto for review. We may reach out to ask for additional information.

All requests for TPM support will be run through the [TPM calculator](https://docs.google.com/spreadsheets/d/1Tv9WK9LIBJgLbEWWMEvXLtewDDv-luJWPjQDtkhWaJ0/edit?gid=466840717#gid=466840717). You can do that yourself to see if your project is likely to receive TPM support. 

## What is the Project?

<!--This information will help an assigned TPM ramp up on your project, so please be as descriptive as possible. No worries if you don't have all this information yet.-->

- Overview / summary: _what can you share about the "what" and "why" of this project?_
- Key stakeholders: _who should the TPM reach out to for more information while ramping onto the project?_
- Stages / teams involved: _include other important teams outside of R&D if relevant_
- Documentation: _any existing documentation about project goals, scope, etc_
- Project slack channel(s):

## When do you need TPM support?

- Urgency: _if known_
- Expected start date: _if known_
- Expected end date: _if known_

/confidential

/labels "TPM Coverage" "TPM Coverage::In Review"

/assign @natalie.pinto