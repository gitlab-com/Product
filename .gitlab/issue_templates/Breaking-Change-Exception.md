<!-- Please review https://docs.gitlab.com/ee/development/deprecation_guidelines/#requesting-a-breaking-change-in-a-minor-release for details of the process. -->
<!-- For any changes/updates to this template, please add the Product Ops DRI as a reviewer. For material updates to the process described in this issue template, approval should be acquired from the CPO & CTO as in the initial creation of the process - https://gitlab.com/gitlab-org/gitlab/-/merge_requests/122626 -->

<!-- The format of the title should be Title should follow the format `Breaking change exception: Description`-->

# Breaking Change Exception Request

## Executive summary

<!-- Three to five sentences. Needs to be easy to follow by C-Level executives. Focus on customer impact -->

## Impact assessment

### How many customers are impacted?

### Can we get the same outcome without a breaking-change?

<!-- What other alternatives were considered? -->

### Can the breaking-change wait till the next major release, or the next scheduled upgrade stop?

### What is the alternative for customers to do the same job the change will break?

### How difficult is it for customers to migrate to the alternative? Is there a migration plan?

## Communication plan

## Tasks

- [ ] Notify Support and Customer Success so they can share information with relevant customers.
- [ ] Obtain approval from the Director of Engineering in the section making the breaking change
- [ ] Obtain approval from the VP of Product Management
- [ ] Obtain approval from the VP of Customer Support
- [ ] Obtain approval from the CPO
- [ ] Obtain approval from the CTO

## Approvals
<!-- Update below with the appropriate names required for approval -->

- [ ] `Director of Engineering - Section containing breaking change`
- [ ] `VP of Product Management`
- [ ] `VP of Customer Support`

### E-Group Approval

Notify the CPO and CTO of this breaking change exception in their respective slack channels `#chief-product-officer` & `#cto` along with their [EBA](https://handbook.gitlab.com/handbook/eba/) so reviewing this request can be scheduled. If the request is urgent, please give a time box for when the request needs to be completed to avoid impacting customer results.

- [ ] `CPO` 
- [ ] `CTO`
