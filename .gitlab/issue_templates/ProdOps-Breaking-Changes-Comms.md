# Overview

With every major release, it's important we [communicate breaking changes](https://about.gitlab.com/handbook/marketing/blog/release-posts/#communicating-breaking-changes) clearly and consistently to all of our users. This is especially important for SaaS customers, as the breaking changes may affect their workflow as early as the 1st day of a major release month, and continue to do so in a rolling fashion until the major release is formally announced on the 22nd day of the month. Follow the task list below to drive communication about breaking changes.

**NOTE:**

**- All due dates in this issue assume a regular release cycle of XX.0 - XX.10 --> XX.00, going from June to May** For example, if 15.0 is being released May 22, you'll be assigned this issue 4 milestones in advance, during 14.7. If minor releases are added or removed, you will need to shift the dates below to align.

## Opening tasks

Perform these tasks when you first open this issue.

- [ ] Based on the planned date of the upcoming major release, update the due dates below.
- [ ] In Slack `#release-post`, make an announcement informing Product Managers about the important days for upcoming major release. (This includes all the regular due dates for the release post and a note that the sooner a breaking change MR/supporting documentation MRs are merged, the [more likely they are to make it into the major release](https://about.gitlab.com/handbook/engineering/releases/#when-do-i-need-to-have-my-mr-merged-in-order-for-it-to-be-included-into-the-monthly-release) and be delayed another year).
- [ ] Cross post this Slack thread across the following channels: `#product #UX #development #quality #eng-managers`


## Kick-off breaking changes communications

### 🗓 Complete by YYYY-MM-20 (4 milestones prior to major release)

- [ ] In Slack `#release-post`, make an announcement informing Product Managers that communication for [breaking changes](https://about.gitlab.com/handbook/product/gitlab-the-product/#breaking-changes-deprecations-and-removing-features) will commence in XX.Y (major - 2), leveraging broadcast messaging to point to the [SSOT for deprecations & removals in Docs](https://docs.gitlab.com/ee/update/deprecations).
  - Remind them to follow the [deprecations and removals process](https://about.gitlab.com/handbook/marketing/blog/release-posts/#deprecations-removals-and-breaking-changes) and partner with [their Technical Writer](https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments) to ensure docs are up to date at all times leading up to the major release.
  - Remind them to check the `removal_date`, `removal_milestone` and `breaking_change` keys in their deprecation and removal YML files, to ensure breaking changes are highlighted accurately in the Docs.
  - Link them to this issue in the post for reference.
  - [ ] Cross post this Slack thread across the following channels:
    - [ ] `#product`
    - [ ] `#UX`
    - [ ] `#tw-team`
    - [ ] `#development`
    - [ ] `#quality`
    - [ ] `#eng-managers`
    - [ ] `#marketing`
    - [ ] `#product-marketing`
    - [ ] `#customer-success`
    - [ ] `#sales`
    - [ ] `#support_gitlab-com`
    - [ ] `#support_self-managed`
    - [ ] `#external-comms`

## Initiate breaking changes blog post and schedule the preliminary broadcast message

### 🗓 Complete by 2022-MM-20 (3 milestones prior to major release)

#### Blog post

- [ ] Follow the process for [requesting](https://about.gitlab.com/handbook/marketing/corporate-marketing/corporate-communications/#requests-for-external-announcements) and [creating](https://about.gitlab.com/handbook/marketing/blog/#how-to-suggest-a-blog-post) a blog post and start one for breaking changes.
  - In the blog issue you create, communicate clearly that the URL for the blog post must be live on the first day of the major release milestone, for example, April 18 for a major release that's launching May 22
  - For reference:
    - [14.0 issue](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/11247)
    - [14.0 MR](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/83586)
    - [14.0 blog post](https://about.gitlab.com/blog/2021/06/04/gitlab-moving-to-14-breaking-changes/) (repurpose the intro, formatting, etc.)
- [ ] Add the blog issue you've created to **Related Issues** for this issue
- [ ] In the blog issue you created, ping the head of Technical Writing `@susantacker` and ask her for a TW partner to help review the final blog post pre-publication
- [ ] In the blog issue you created, ping the blog DRI `@vsilverthorne` and notify them of your target publication date and request clarity on a date to submit for legal review

#### Broadcast message

- [ ] Follow the process for scheduling a GitLab.com [broadcast message](https://about.gitlab.com/handbook/product/product-processes/#gitlabcom-broadcast-messages-broadcast-messaging):
  - Create a [broadcast message issue](https://gitlab.com/gitlab-com/Product/-/issues/new?issuable_template=PM-in-app-messaging-request)
  - Use this copy for the broadcast message: `The XX.Y major release is coming up! This version brings many exciting improvements to GitLab, but also removes some deprecated features. Visit the [deprecations page](https://docs.gitlab.com/ee/update/deprecations) to see what is scheduled for removal in XX.Y, and check for any **breaking changes** that could impact your workflow.`
  - In the broadcast message issue, communicate these requirements:
    - The message initiates on the 18th of the month for milestone XX.Y (major - 2)
    - After the user closes the message, it should not reappear again
    - Target path is `*/*`
  - Add the broadcast message issue you've created to **Related Issues** for this issue
  - For reference, here is the [Broadcast message issue for 15.0](https://gitlab.com/gitlab-com/Product/-/issues/3827)
- [ ] In Slack `#release-post`, announce to the Product Managers that broadcast messages will start running for breaking changes XX.Y (major - 2) onward. Remind them they need to make sure that [SSOT deprecations and  removals in Docs](https://docs.gitlab.com/ee/update/deprecations) is up to date as that's the source for automation.
  - Link them to the broadcast message issue for reference.
  - [ ] Cross post this Slack thread across the following channels:
    - [ ] `#product`
    - [ ] `#UX`
    - [ ] `#tw-team`
    - [ ] `#development`
    - [ ] `#quality`
    - [ ] `#eng-managers`
    - [ ] `#marketing`
    - [ ] `#product-marketing`
    - [ ] `#customer-success`
    - [ ] `#sales`
    - [ ] `#support_gitlab-com`
    - [ ] `#support_self-managed`
    - [ ] `#external-comms`

## Monitor breaking changes blog and schedule the major release broadcast message

### 🗓 Complete by 2022-MM-20 (2 milestones prior to major release)

- [ ] Do follow-ups as needed in your blog issue to ensure the link will go live on the first day of the major release milestone, for example, April 18 for a major release that's launching May 22.
  - It's important the live URL be available by the 18th of the month as you'll schedule the broadcast message to start the 20th of the month, to ensure SaaS users are aware before breaking changes start getting merged and affect their workflow
- [ ] Check in with Product Operations `@fseifoddini` as to whether there's any reason to not announce the breaking changes starting on the 20th of the major milestone month.
  - Some teams may have special considerations that warrant publishing the blog at a later date.
- [ ] Post a reminder to PMs in Slack `#release-post` to ensure breaking changes are appearing accurately in [Deprecations and Removals](https://docs.gitlab.com/ee/update/deprecations) as that will be the SSOT for broadcast messaging and generating the breaking changes blog.
  - cross post in `#product #eng-managers #tw-team`

#### Broadcast message

- [ ] Follow the process for scheduling a GitLab.com [broadcast message](https://about.gitlab.com/handbook/product/product-processes/#gitlabcom-broadcast-messages-broadcast-messaging)
  - Use this copy for the broadcast message: `The XX.Y version of GitLab is launching between now and MM DD! There are breaking changes in this major release. This version brings many exciting improvements to GitLab but also removes deprecated features. Please visit {Title of breaking changes blog} and Removals to see what is being removed in XX.Y, and which **breaking changes** may impact your workflow.`
  - In the broadcast message issue you create, communicate these requirements:
    - Message initiates on the 20th of the month for milestone XX.0
    - After the user has closed the message, it should not appear again
- [ ] In Slack `#release-post`, make an announcement informing Product Managers that final broadcast messages for breaking changes will run in XX.0 starting on the 20th of the month
  - Link them to the broadcast message issue for reference
  - [ ] Cross post this Slack thread across the following channels:
    - [ ] `#product`
    - [ ] `#UX`
    - [ ] `#tw-team`
    - [ ] `#development`
    - [ ] `#quality`
    - [ ] `#eng-managers`
    - [ ] `#marketing`
    - [ ] `#product-marketing`
    - [ ] `#customer-success`
    - [ ] `#sales`
    - [ ] `#support_gitlab-com`
    - [ ] `#support_self-managed`
    - [ ] `#external-comms`

## Finalize breaking changes blog and finalize content/URLs for the major release broadcast message

### 🗓 Complete by 2022-MM-01 (1 milestone prior to major release)

- [ ] Work with Product Operations `@brhea` to populate the right list of breaking changes into the breaking changes blog MR
- [ ] In Slack `#release-post` share the MR/View app with the PM team and ask them to make sure none of their breaking changes are missing.
  - If so, they will need to update their deprecation yml files in Docs and request an update to the blog post from Product Operations `@brhea`
  - Cross post in Slack `#product #eng-managers #quality #docs`
- [ ] Repeat the steps above again sometime around the 10th of the month by following up in the Slack thread you created above

## Launch and monitor breaking changes blog and major release broadcast message

### 🗓 Complete by 2022-MM-16 (1 milestone prior to major release)

- [ ] Be sure your blog is getting published on time!
- [ ] As soon as you have the live URL link, add it to the broadcast message issue you created for the major release

## Update Breaking Changes blog with Removals content

### 🗓 Complete on 2022-MM-17 (lead up to major release)

#### Update blog with Removals content

- [ ] Compile Removals and update the Breaking Changes content in a new Draft MR `@brhea`
- [ ] In Slack `#release-post`, share the View App URL and notify Product Team to preview the updated content

### 🗓 Complete on 2022-MM-18 (lead up to major release)

#### Collect PM feedback and recompile

- [ ] Update the blog content based on PM feedback and review
- [ ] Merge updated removal content

### 🗓 Complete on 2022-MM-19 (lead up to major release)

#### Final broadcast message

- [ ] Confirm final broadcast message is deployed

/label ~"Product Operations" ~"workflow::In dev"
/assign @fseifoddini @brhea
