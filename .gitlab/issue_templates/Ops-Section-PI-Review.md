## :book: References

* [Handbook Page](https://internal-handbook.gitlab.io/handbook/company/performance-indicators/product/ops-section/)
* [PI YAML Data Fields](https://about.gitlab.com/handbook/engineering/performance-indicators/#data)
* [PI Implementation Status Reference](https://about.gitlab.com/direction/product-analytics/#implementing-product-metrics)
* [Meeting Notes](https://docs.google.com/document/d/1ff7V_SRdJDYWzfCMAFLGYWxrMHAlTds4No6ROQZP2jo/edit#)
* [Past Prep Issues](https://gitlab.com/gitlab-com/Product/-/issues?scope=all&utf8=%E2%9C%93&state=all&label_name[]=PI%20Review%20Prep&label_name[]=section%3A%3Aops)

## :dart: Intent

* [Ops Section PIs - Intent](https://internal-handbook.gitlab.io/handbook/company/performance-indicators/product/ops-section/#intent)

## :white_check_mark: Tasks

### :o: Opening Tasks

<!-- * [ ] Add stub MR for PI updates - `@section-leader` -->

* [x] Set a due date to this issue as 2 business days prior to the scheduled review to make time for Leadership async review -  @jreporter 
* [x] Add a retrospective thread to this issue -  @jreporter 
* [x] Update the [Meeting Notes](https://docs.google.com/document/d/1ff7V_SRdJDYWzfCMAFLGYWxrMHAlTds4No6ROQZP2jo/edit#) with the timing and this Prep Issue - @jreporter 
* [x] Check remaining follow ups from [last review](https://gitlab.com/gitlab-com/Product/-/issues?scope=all&utf8=%E2%9C%93&state=all&label_name[]=PI%20Review%20Prep&label_name[]=section%3A%3Aops) -  @jreporter 
* [ ] Coordinate timing of reviews from Directors and Senior Product Design Managers `@mflouton @sgoldstein @hbenson @jreporter @mvanremmerden`-  @jreporter 
* [ ] Review the [update actions](#star-performance-indicator-yaml-updates) and ensure they are accurate per the [Data definitions](https://internal-handbook.gitlab.io/product/performance-indicators/ops-section/#data) -  @jreporter 

### :clock3: Quarterly Tasks
* [ ] If the first month of the quarter - Start a new discussion thread with team about how they set and attained their previous quarter results/PI targets/OKRs -  @jreporter 
* [ ] If the first month of the quarter - Add a reminder to include both their previous quarter and next quarter targets and discuss quarterly attainment in their insights/health as well as progress to annual targets -  @jreporter 

### :star: Performance Indicator YAML Updates

Each product stage or group should update the [Performance Indicators YAML file](https://gitlab.com/internal-handbook/internal-handbook.gitlab.io/-/blob/main/data/performance_indicators/ops_section.yml) with:

* Update your `Health:` and `Instrumentation:` levels
* Update any `URLs:` to reference stage/group direction or handbook pages for the rationale, growth model and funnel
* Update any `URLs:` to reference additional Sisense dashboards for more details
* Update `Implementation Status:` with the current maturity
* Update `Lessons Learned:` with your takeaways from the previous month's data
* Update `Focus This Month:` with your plan for the month ahead 

### :star: Expectations
1. Ensure your graph is refreshed
1. Ensure your graph looks correct and if it's not, understand why and change it
1. Have a comprehensive understanding of your graphs (understand history, challenges with the data, etc) and document directly in the .yml file
1. Ensure there are no graph labeling or title errors
1. Ensure you have a target set and you know how that target was set. If you can't have a target, in the target field, please explain why you don't have a target and when you would expect to have one.

### :star: Trend Review
1. Review [Prioritization dashboard](https://app.periscopedata.com/app/gitlab/1042933/Issue-Types-by-Milestone) & [MR type dashboard](https://app.periscopedata.com/app/gitlab/976854/Merge-Request-Types-Detail) filtered for team_group on:

[+ - To be added by Section Leader +]

1. Answer questions in monthly meeting agenda regarding dashboard data accuracy, error budgets, and prioritization trends

#### Group Updates

[+ - To be added by Section Leader +]

### :white_check_mark: Checkboxes

Section level update

- [ ] on CMAU - `@jreporter`
- [ ] on Error Budget, Security, Infradev - `@sgoldstein` - ([add any MR updates](https://internal-handbook.gitlab.io/handbook/company/performance-indicators/product/ops-section/))
- [ ] on Say/Do ratios - `@sgoldstein`
- [ ] on SUS - `@mvanremmerden` 
- [ ] on OCBA S1/S2, Overall Bug hygiene - `@jo_shih`/`@svistas` 

<<<<<<< .gitlab/issue_templates/Ops-Section-PI-Review.md

Say/do ratios (to be completed by group EM)

* [ ] Verify:Pipeline Execution - @carolinesimpson - *Ratio for past milestone* / *Note or link to async update*
* [ ] Verify:Pipeline Authoring - @nmezzopera  - *Ratio for past milestone* / *Note or link to async update* 
* [ ] Verify:Runner - @nicolewilliams   - *Ratio for past milestone* / *Note or link to async update*
* [ ] Package - @crystalpoole  - *Ratio for past milestone* / *Note or link to async update*
* [ ] Deploy - @nmezzopera  - *Ratio for past milestone* / *Note or link to async update*

<details><summary>Deploy stage</summary>
>>>>>>> .gitlab/issue_templates/Ops-Section-PI-Review.md

- [ ] `@nagyv-gitlab` 
- [ ] @rayana 
- [ ] `@nmezzopera`

</details>

<details><summary>Package stage</summary>

- [ ] `@jreporter`
- [ ] `@trizzi` 
- [ ] `@rayana` 
- [ ] `@crystalpoole`

</details>


<details><summary>Verify stage</summary>

- [ ] `@jreporter`
- [ ] `@DarrenEastman`
- [ ] `@dhershkovitch` 
- [ ] `@jocelynjane`
- [ ] `@cheryl.li`
- [ ] `@nicolewilliams`
- [ ] `@gabrielengel_gl` 
- [ ] `@rayana`

</details>

### :package: Pre-Meeting Tasks
* [ ] Consider pre-recording an overview -  @jreporter 
* [ ] Request Section Leader review -  @jreporter 
   * [ ] `@hbenson`
   * [ ] `@sgoldstein`
   * [ ] `@mvanremmerden`
   * [ ] `@jreporter`
* [ ] Ensure ample time for Product Manager and Engineering Manager review of all stages - @jreporter 
   - [ ] `@jreporter`
   - [ ] `@sgoldstein`
* [ ] Ask questions in the [doc](https://docs.google.com/document/d/1ff7V_SRdJDYWzfCMAFLGYWxrMHAlTds4No6ROQZP2jo/edit#) -  @jreporter 
* [ ] Pay special attention to progress towards annual goals for CMAU and SMAU -  @jreporter 
* [ ] Share the updated PI handbook page in the #ops-section and #product Slack channels 2 business days in advance of the review and encourage async participation -  @jreporter 

### :night_with_stars: Post-Meeting Tasks
* [ ] Capture actions discussed in the review in the [follow-up actions](#follow-up-actions) section -  @jreporter 

### :x: Closing Tasks
* [ ] Activate on any retrospective thread items -  @jreporter 
* [ ] Add a reminder to team members to capture their follow-ups 
in issues and add them to the description -  @jreporter 
* [ ] Make [adjustments to the template](https://gitlab.com/gitlab-com/Product/-/edit/main/.gitlab/issue_templates/Ops-Section-PI-Review.md) based on the retrospective thread -  @jreporter 
* [ ] Add a highlights comment and share in Slack -  @jreporter 
* [ ] Based on any adjustments to PI targets from individual stages and groups - adjust overall CMAU targets ([example](https://gitlab.com/gitlab-com/Product/-/issues/3443)) -  @jreporter 

### Follow Up Actions
* [ ] ACTION - `@assignee`

/assign @dhershkovitch @DarrenEastman @trizzi @jreporter @nagyv-gitlab @jocelynjane @hbenson @sgoldstein @mflouton @mvanremmerden @gabrielengel_gl
/label ~"section::ops"  ~"PI Review Prep"

