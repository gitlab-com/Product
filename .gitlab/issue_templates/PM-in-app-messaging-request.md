
Before requesting a broadcast message, please be sure to read the latest [guidance](https://about.gitlab.com/handbook/product/product-processes/#gitlabcom-in-app-messages-broadcast-messaging) in the product handbook. 

## Request for recruiting or otherwise communicating with users via Broadcast Messages

When possible, please create this issue and push it into the approval workflow 30 calendar days prior to your desired "go live" date. This will allow collaborators enough time to support your necessary timing.

Ensuring a positive user experience for our users is the most important factor to consider when deploying messaging in our product. With that goal in mind, we have put in place the following procedures to ensure that in-app messaging does not result in any negative user sentiment.

### General in-app messaging guidelines

- Messages must be approved by PLT (one VP of Product and the VP of UX) in order to run. If a VPP is not available and the request is time-sensitive, `@justinfarris` can review and approve. 
- We run a maximum of 2 in-app messages at any given time in order to limit the number of messages a user sees in quick succession.
- Messages run for the shortest amount of time possible.
- Messages are limited to a single page on the site unless there is a business reason to put it on multiple pages.
- Messages should apply to all users that see them. 
- When possible, please create this issue and push it into the approval workflow 30 calendar days prior to your desired "go live" date. This will allow collaborators enough time to support you.

If you need to target specific users with your message, or you require action from the user, consider an MR using an [alert](https://design.gitlab.com/components/alert) or [banner](https://design.gitlab.com/components/banner).

### Broadcast Message Capability

- Broadcast Messages are for Gitlab.com.
- You can specify a user role for message targeting but other user attributes are not available at this time.
- You can select a [banner or a notification type](https://docs.gitlab.com/ee/user/admin_area/broadcast_messages.html#banners) - note that banner messages currently also display messages in `Git remote responses` by default in the configuration template. To prevent your broadcast message from displaying in `Git remote responses` be sure to denote this in your request form. 
- You can specify a page/s to display the message on. Current functionality is unable to support targeting a specific list of groups or projects at this time. 
- You can specify whether the message is dismissable or not. It is advised that messages be configured as dismissable unless the content warrants non-dismissal as determined by PLT. 
- Broadcast messages and notifications are displayed on a user-level basis. Display and dismissal are recorded on the user level which means that an individual user's dismissal of a broadcast message will not dismiss the message for other users of same group or role, depending on selected target criteria.  
- You can utilize URLs to direct users to webpages with more information about your announcement and need to be generalized. URLs cannot be relative to the user's instance at this time.

## Tasks for Product Operations
- [ ] Apply label `workflow::In review` to this issue
- [ ] Review, prioritize and approve this issue by viewing other pending and active broadcast messages on the [scheduling board](https://gitlab.com/gitlab-com/Product/-/boards/1889145?label_name%5B%5D=In-App-Messaging)
- [ ] Apply a `Priority 1` or `Priority 2` label, and tag the Requester and comment with any additional feedback or guidance you have

## Tasks for Requester

### After you've answered the [questions below](#proposed-broadcast-message)

- [ ] Assign this issue to `@justinfarris` & `ampesta` for review and approval. Tag the Requester in a comment with a link to #product

#### After Product Operations has approved

- [ ] Apply label `workflow::In dev`
- [ ] Assign to your [Stage UX Researcher](https://about.gitlab.com/handbook/engineering/ux/ux-research/#ux-researcher-assignments) if you need help with messaging content, target pages, target users, etc. for messages related to recruiting participants for UX research.
- [ ] Source a UX team member to provide UX review and approval. This is required prior to the message start date.
- [ ] Source a TechWriting team member to provide TW review and approval. This is required prior to the message start date.
- [ ] Once you know the message details, add label `workflow::scheduling` and tag `@gdoud` to schedule it.

#### Once the message goes live

- [ ] Apply label `workflow::production` and leave this issue open while the message is running and live to users
- [ ] Set a reminder for yourself to `close` this issue on the end date of the broadcast message 

### Proposed Broadcast Message
Please fill out as much as you can - you can add details later - 

#### Type of message?
- Informational (no CTA)
- Link (copy & CTA)
- Survey (copy & in-line question)

#### What should the message say?

#### Would you like to run a [banner](https://docs.gitlab.com/ee/user/admin_area/broadcast_messages.html#banners) or a [notification](https://docs.gitlab.com/ee/user/admin_area/broadcast_messages.html#notifications)? (Please note that notifications are limited just one running at a time).

##### For a banner, what should the background color be?

##### For a banner, should the message be dismissable?

#### Which user roles should see this message? 

#### What pages (target path) should the message be displayed on (Paths can contain wildcards, like */welcome)?

#### What is the start date/time in UTC?

#### What is the end date/time in UTC?

#### Is this for user research? (If yes, please link your research issue from the [UX Research project](https://gitlab.com/gitlab-org/ux-research)


/label ~In-App-Messaging ~"Product Operations"
/assign @justinfarris @gdoud
