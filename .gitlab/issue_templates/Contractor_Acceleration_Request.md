## Purpose
<!-- A short summary of  the business case -->

### Background

<!-- Why do we need to bring in external contractors rather than use the existing team-->

### Number of contractors
<!-- How many people will we need to bring in and what expertise (UX/FE/BE..)-->

### Justification
<!-- What will be the impact if this is done-->

### Cost

<!-- What will be the cost to do this. Prepare with bringing in a quote from the vendor-->

### Duration

<!-- How long will this take? -->

### Budget

<!-- What budget is this coming from-->

### DRI to lead the effort from the team

### Goal
<!-- What is the end goal of this project?-->

### How will we measure success?

### Approvals

* [ ] PM
* [ ] EM
* [ ] Director of Product Design
* [ ] GPM/Director of Product
* [ ] SEM/Director of Engineering 

