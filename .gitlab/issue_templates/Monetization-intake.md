# Monetization Requests - Intake Process
#### Process Purpose & Overview

This intake process is meant to standardize the pricing, promotion, partnership, and other monetization requests that are received by Product, Legal, Billing, and other stakeholders.

#### Process Steps
1. Requestor submits request via this template
2. Request is recieved and reviewed by cross-functional at Pricing Committee meetings (monthly)
3. Requeset is prioritized by the Committee
4. Prioritization status is communicated to Requestor
<br> _Please note that submitting an issue does not automatically mean that the idea will be pursued._

# {+Requestor Section Start+} 
### 1. Requestor Details
- [ ] Name: `Your Name Here`
- [ ] Slack handle: `@yournamehere`
- [ ] Team: `Your team here`

<!-- blank line -->
----
<!-- blank line -->

### 2. Request Description & Business Justification
Provide a description of the request or proposal. Including: What the request is and why would this be of value to GitLab.

#### 2a. Which functions would mainly drive executing this request? (choose _one_)
- [ ] Product
- [ ] Legal
- [ ] Revenue
- [ ] FP&A 
- [ ] Billing
- [ ] Other // If other `provide function`

#### 2b. Which functions would support executing this request? (choose as many that apply)
- [ ] Product
- [ ] Legal
- [ ] Revenue
- [ ] FP&A 
- [ ] Billing
- [ ] Other // If other `provide function(s)`

<!-- blank line -->
----
<!-- blank line -->
### 3. Request ROI
#### 3a. What would be the main success measure for this request if it were executed?
Provide the KPIs that would part of measuring whether this work was successful. TAM, ARR, nARR, strategic justification, etc.

#### 3b. What would be the expected revenue return if this request were executed?
`$          `

#### 3c. Are you (or your function) willing to commit to a revenue target if this request were executed?
- [ ] Yes
- [ ] No
<br> _If no, why not:_ `Provide more infomration on why not`

<!-- blank line -->
----
<!-- blank line -->
### 4. Implementation Information
#### 4a. In what quarter would this request / opportunity ideally be completed?
- [ ] Q1 FY25
- [ ] Q2 FY25
- [ ] Q3 FY25
- [ ] Q4 FY25

#### 4b. Would this effort require development / engineering resources?
- [ ] Yes
- [ ] No

#### 4c. Would this effort require operational enablement or systems configuration?
- [ ] Yes
- [ ] No

#### 4d. Does this request require non-standard conract or transaction terms?
- [ ] Yes
- [ ] No

#### 4e. Does this request pertain to a specific customer segment? If yes - provide below
- [ ] Enterprise
- [ ] MM
- [ ] SMB
- [ ] PubSec

#### 4f. Does this request pertain to a marketplace?
- [ ] Yes // _If yes, which one?_ `provide here`
- [ ] No

#### 4g. Does this request pertain to a partnership?
- [ ] Yes // _If yes, which one?_ `provide here`
- [ ] No

<!-- blank line -->
----
<!-- blank line -->

# {+ Reviewer Section Start+} 
### 1. Reviewed by
- [ ] **Product**
<br> Reviewed by: `@taginreviewer`
<br> `Functional feedback or questions recorded here`

- [ ] **Legal**
<br> Reviewed by: `@taginreviewer`
<br> `Functional feedback or questions recorded here`

- [ ] **Revenue**
<br> Reviewed by: `@taginreviewer`
<br> `Functional feedback or questions recorded here`

- [ ] **FP&A**
<br> Reviewed by: `@taginreviewer`
<br> `Functional feedback or questions recorded here`

- [ ] **Billing**
<br> Reviewed by: `@taginreviewer`
<br> `Functional feedback or questions recorded here`

- [ ] **Other** // If other `provide function`
<br> Reviewed by: `@taginreviewer`
<br> `Functional feedback or questions recorded here`


<!-- blank line -->
----
<!-- blank line -->
# Do Not Edit Below
<!-- DO NOT EDIT BELOW -->
/confidential
<br> /label  ~"Monetization Intake::Not Started"