## Introduction

This issue serves as a reminder to update the lunch and learn deck created for Customer Success Managers for Enteprise Agile Planning use cases on a quarterly basis.

Updates to this template can be made [here](.gitlab/issue_templates/Plan-CSM-Enablement-Review.md)

## :white_check_mark: Tasks

Product Management: 

- [ ] Review the features released in the last quarter - [Link](https://gitlab-com.gitlab.io/cs-tools/gitlab-cs-tools/what-is-new-since/?tab=features) 
- [ ] Determine which slides in the lunch and learn deck need to be updated -  [Link](https://docs.google.com/presentation/d/1R91frGFXm6g8_cpndgklYkzY-E1nw6qEv9hnZfpuk7E/edit?usp=sharing)
- [ ] Determine if the video walkthrough needs to be re-recorded - [Link](https://www.youtube.com/watch?v=psh46ZOccHs)
- [ ] Ping CSM counterparts when updates are completed -  @marygracewajda @rachel_fuerst 

CSM:

- [ ] Update the deck based on updated branding if applicable - [Link](https://docs.google.com/presentation/d/1R91frGFXm6g8_cpndgklYkzY-E1nw6qEv9hnZfpuk7E/edit?usp=sharing)
- [ ] Determine if additional content is needed based on feedback from other field team members
- [ ] Review updates from PM

/assign @mushakov @gweaver @amandarueda @marygracewajda @rachel_fuerst 
/due in 15 days
