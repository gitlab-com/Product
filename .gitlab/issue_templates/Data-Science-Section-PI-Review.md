## :book: References

* [Handbook Page](https://internal.gitlab.com/handbook/company/performance-indicators/product/data-science-section/)
* [Meeting Notes](https://docs.google.com/document/d/1_PG3c7sLXbekceXOzC_vFJ4vYXFNl-RGEiV1OU58Z_w/edit#)
* [PI .yml file](https://gitlab.com/internal-handbook/internal-handbook.gitlab.io/-/blob/main/data/performance_indicators/data_science_section.yml)

## :dart: Intent
We are organizing for and regularly reviewing Performance Indicators (PIs) in order to enable a dialog between ourselves (all of R&D) about most effective use of our R&D efforts towards the most impactful improvements.

As a communication tool, Performance Indicators are only as useful as the range of the audience. As a result we should:

* store PI metrics, funnels, growth models, status and next steps handbook first (avoid content only living in slides)
* expose our product groups to PI metrics regularly
* communicate transparently about adjustments to PIs within our groups
* reference PIs in planning issues and prioritization discussions

## :white_check_mark: Tasks

### :o: Opening Tasks

* [ ] Set a due date on this issue as the last business day of the week prior to the scheduled review - @tmccaslin
* [ ] Add a restrospective thread to this issue - @tmccaslin
* [ ] Add stub MR for PI updates - @tmccaslin
* [ ] Prep meeting agenda - @tmccaslin

### :star: Performance Indicator YAML Updates

Each product stage or group should update the [Performance Indicators YAML file](https://gitlab.com/internal-handbook/internal-handbook.gitlab.io/-/blob/main/data/performance_indicators/data_science_section.yml) with:

* Update your `Health:` and `Instrumentation:` levels
* Update any `URLs:` to reference stage/group direction or handbook pages for the rationale, growth model and funnel
* Update any `URLs:` to reference additional Sisense dashboards for more details
* Update `Implementation Status:` with the current maturity
* Update `Lessons Learned:` with your takeaways from the previous month's data
* Update `Focus This Month:` with your plan for the month ahead 

### :star: Expectations
1. Ensure your graph is refreshed
1. Ensure your graph looks correct and if it's not, understand why and change it
1. Have a comprehensive understanding of your graphs (understand history, challenges with the data, etc) and document directly in the .yml file
1. Ensure there are no graph labeling or title errors
1. Ensure you have a target set and you know how that target was set. If you can't have a target, in the target field, please explain why you don't have a target and when you would expect to have one.

### :star: Trend Review
1. Review [Prioritization dashboard](https://app.periscopedata.com/app/gitlab/1042933/Issue-Types-by-Milestone) & [MR type dashboard](https://app.periscopedata.com/app/gitlab/976854/Merge-Request-Types-Detail) filtered for team_group on:
    - AI Model Vadliation 
    - AI Framework
    - Duo Chat
    - ModelOps
1. Answer questions in monthly meeting agenda regarding dashboard data accuracy, error budgets, and prioritization trends
#### Group Updates

**Data Science Section**
* [ ] Section CMAU @tmccaslin
* [ ] Section Dev trend review - @bartek, after content written by @m_gill
  * [ ] @oregand
  * [ ] @mray2020
* [ ] Section PD trend review - @jmandell
* [ ] Section Quality trend review - @vincywilson 

**AI-powered**

* [ ] Overall AI-powered Review @tmccaslin
* [ ] AI Model Validation @tmccaslin
* [ ] AI Framework @tlinz
* [ ] Duo Chat @tlinz
* [ ] PD trend review @jmandell

**ModelOps**
* [ ] Overall ModelOps Review @tmccaslin

### :x: Closing Tasks
* [ ] Make [adjustments to the template](https://gitlab.com/gitlab-com/Product/-/blob/main/.gitlab/issue_templates/Data-Science-Section-PI-Review.md) based on the retrospective thread - @tmccaslin
* [ ] Activate on any retrospective thread items - @tmccaslin

/assign @tmccaslin @jmandell @tlinz @m_gill @mray2020 @oregand
