# Introduction

The Plan stage cross-functional teams gather once a month to review:

* OKRs
* Product usage metrics
* Product availability metrics
* Backlog health

This issue serves as a reminder to prepare updates for each group. Items should be added to the meeting agenda only if there are items out of the ordinary that should be discussed synchronously.

## Tasks

Agenda link: https://docs.google.com/document/d/14ffiBmks5ZgWmJG66JkfBouQWUooHUMJOnH6F1gwXaU/edit#heading=h.ddxrsn7txdcf

Optimize
* [ ] OKRs
* [ ] Product usage metrics
* [ ] Product availability metrics
* [ ] Backlog health

Knowledge
* [ ] OKRs
* [ ] Product usage metrics
* [ ] Product availability metrics
* [ ] Backlog health

Product Planning
* [ ] OKRs
* [ ] Product usage metrics
* [ ] Product availability metrics
* [ ] Backlog health

Project Management
* [ ] OKRs
* [ ] Product usage metrics
* [ ] Product availability metrics
* [ ] Backlog health

/assign @mushakov @donaldcook @blabuschagne @vshushlin @mmacfarlane @johnhope @gweaver @hsnir1 @amandarueda
 