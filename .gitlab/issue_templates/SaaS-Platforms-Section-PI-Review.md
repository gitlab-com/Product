## :book: References

* [Handbook Page](https://internal.gitlab.com/handbook/company/performance-indicators/product/saas-platforms-section/)
* [Meeting Notes](https://docs.google.com/document/d/1QUDdmi3lnLGLFPjUFkxO9d01RiPrNbzt2sFN4ck-6gk/edit)
* [PI .yml file](https://gitlab.com/gitlab-com/content-sites/internal-handbook/-/blob/main/data/performance_indicators/saas_platforms_section.yml)

## :dart: Intent

We are organizing for and regularly reviewing Performance Indicators (PIs) in order to enable a dialog between ourselves (all of R&D) about most effective use of our R&D efforts towards the most impactful improvements.

As a communication tool, Performance Indicators are only as useful as the range of the audience. As a result we should:

* store PI metrics, funnels, growth models, status and next steps handbook first (avoid content only living in slides)
* expose our product groups to PI metrics regularly
* communicate transparently about adjustments to PIs within our groups
* reference PIs in planning issues and prioritization discussions

The DRI for the SaaS Platforms Section PI Review is @fzimmer

## :white_check_mark: Tasks

### :o: Opening Tasks

* [ ] Set a due date on this issue as the last business day of the week prior to the scheduled review - @cbalane
* [ ] Add a retrospective thread to this issue - @cbalane
* [ ] Update the [Meeting Notes](https://docs.google.com/document/d/1QUDdmi3lnLGLFPjUFkxO9d01RiPrNbzt2sFN4ck-6gk/edit) with the time and this Prep issue - @cbalane
* [ ] Prep meeting agenda - @cbalane

### :star: Performance Indicator YAML Updates

Each product stage or group should update the [Performance Indicators YAML file](https://gitlab.com/gitlab-com/content-sites/internal-handbook/-/blob/main/data/performance_indicators/saas_platforms_section.yml) with:

* Update your `Health:` and `Instrumentation:` levels
* Update any `URLs:` to reference stage/group direction or handbook pages for the rationale, growth model and funnel
* Update any `URLs:` to reference additional Sisense dashboards for more details
* Update `Implementation Status:` with the current maturity
* Update `Lessons Learned:` with your takeaways from the previous month's data
* Update `Focus This Month:` with your plan for the month ahead

### Group Updates

* [ ] Dedicated - @cbalane - 
* [ ] US Public Sector Services - @cbalane - 
* [ ] Switchboard - @lbortins - 
* [ ] Delivery - @mbruemmer -
* [ ] Scalability - @swiskow -
* [ ] Section-wide - @fzimmer - 

### :x: Closing Tasks

* [ ] Make [adjustments to the template](https://gitlab.com/gitlab-com/Product/-/blob/main/.gitlab/issue_templates/SaaS-Platforms-Section-PI-Review.md) based on the retrospective thread - @cbalane
* [ ] Activate on any retrospective thread items - @cbalane

cc @o-lluch @ashiel @denhams @dawsmith @mbursi @kwanyangu @lmcandrew @rnienaber @amyphillips @marin @andrewn @eneuberger 

/assign @fzimmer @cbalane @swiskow @lbortins @mbruemmer
/epic https://gitlab.com/groups/gitlab-com/-/epics/2279
/label ~"Sub-Department::SaaS Platforms" 
