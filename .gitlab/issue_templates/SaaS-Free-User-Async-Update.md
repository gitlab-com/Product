## :dart: Intent

Ensure the SaaS Free User Async Update is collected twice monthly

## :book: References

1. [Direction](https://internal-handbook.gitlab.io/handbook/product/saas-efficiency/direction/)
1. [Async Update Process](https://internal-handbook.gitlab.io/handbook/product/saas-efficiency/direction/#async-updates)
1. [DRI Table](https://internal-handbook.gitlab.io/product/saas-efficiency/direction/#sub-project-dris)

## Request Template

```
@DRI - Please remember to provide your twice monthly [async update](https://internal-handbook.gitlab.io/product/saas-efficiency/direction/#async-updates) here by 4:00pm Pacific Time (23:00 UTC) Tuesday.

Please use the following format for the update:

>>>
## Twice Monthly Async Update - YYYY-MM-DD

1. **Planned Launch Date (or N/A):** YYYY-MM-DD. If this changed from the last update, please note the previously planned launch date.
1. **On/Off Track? (or N/A)** Please list key risks if off track.
1. **New risks since the last update?**
   1. New risk
   1. New risk
1. **Results/Challenges/Learnings:**
   1. Results:
   1. Challenges:
   1. Learnings:

FYI @ 
>>>

Thanks!
```

## Summary Template

>>>
# Twice Monthly SaaS Free User Efficiency Initiative Update

## :thinking: Customer Insights Recently Learned

1. Include any customer insights learned over the past two weeks

## :tada: Updates

List by Sub-Project and include the Sub-Project DRI. e.g.:

1. [**New Free Tier User Limits**](https://gitlab.com/groups/gitlab-com/-/epics/1707) (Sam Awezec)
   1. **Planned Launch Date (or N/A):**
   1. **On/Off Track? (or N/A)**
   1. **New risks since the last update?**
      1. New risk
      1. New risk
   1. **Results/Challenges/Learnings:**
      1. Results:
      1. Challenges:
      1. Learnings:

## :key: Key Free User Offer Adjustment Sub-Project Status

1. User Limits in-app notifications and enforcement for all free SaaS - net new: :white_check_mark: 2022-12-28, existing namespaces: :white_check_mark: began rollout 2023-06-05, finished 2023-06-13. See the [enforcement rollout](https://gitlab.com/gitlab-org/gitlab/-/issues/386610#enforcement-rollout) section of the user limits rollout issue for the timeline.
   1. The 5-user limit has been launched for net new namespaces created on/after 2022-12-28
   1. Enforcement for all existing namespaces that are only over the user limit (these namespaces are not over the future storage limit) began rolling out on 2023-06-05 and finished on 2023-06-13
1. Storage Limits in-app notifications and enforcement for all free SaaS - :warning: TBD
   - Storage limits is targeting end of FY24-Q2 to enable storage limit notifications for free namespaces and 60 days into FY24-Q3 for enforcement of storage limits for free namespaces
>>>

## Tasks

### :wave: Open

- [ ] @ipedowitz - Create the `♻ Retro Thread` as a comment to this issue

### :exclamation: Extras

- [ ] @ipedowitz - If first week of the month - Ask additional probing questions when requesting updates

### :envelope: Monday Tasks

1. [ ] @ipedowitz - Open [all artifacts in the DRI Table](https://internal-handbook.gitlab.io/product/saas-efficiency/direction/#sub-project-dris) and use the [request template](#request-template)) to request an update
   - [ ] [Data Transfer Limits for GitLab.com](https://gitlab.com/groups/gitlab-com/-/epics/1664) updates are monthly on the first week of the month
1. [ ] @ipedowitz - Ensure you have a block of time to draft the update Tuesday afternoon at 5:00pm Pacific Time (00:00 UTC)

### :eyes: Tuesday Tasks

- [ ] @ipedowitz - Open [all artifacts](https://internal-handbook.gitlab.io/product/saas-efficiency/direction/#sub-project-dris) to review and summarize in a comment on this issue

### :pencil2: Wednesday Tasks

1. [ ] @ipedowitz - Add the update as a comment on the [initiative Epic](https://gitlab.com/groups/gitlab-com-top-initiatives/free-saas-user-efficiency/-/epics/1) and ping `FYI @gl-free-saas-user-efficiency-leaders @akramer @justinfarris @ronell @david` by 10:00am Pacific Time (17:00 UTC)
1. [ ] @ipedowitz - Add `🎉 Updates` to the description of the [initiative Epic](https://gitlab.com/groups/gitlab-com-top-initiatives/free-saas-user-efficiency/-/epics/1) and update `📆 Key Milestones` as needed

### :mega: Communicate

1. [ ] @ipedowitz - Communicate the async update in the [#i_efficient_free_tier](https://app.slack.com/client/T02592416/C039T93DXAL) Slack channel (ping ahead of the Wednesday Sync review) by posting a screenshot of the update from [:pencil2: Wednesday Tasks](#pencil2-wednesday-tasks) and commenting `@Ashley @Craig Mestel @david Twice Monthly Async Update - https://gitlab.com/groups/gitlab-com-top-initiatives/free-saas-user-efficiency/-/epics/1`
   1. [ ] Update the link to point directly to the appropriate comment
1. [ ] @ipedowitz - Add an agenda item for the twice monthly update in the [SaaS Free User Efficiency - Standup Agenda](https://docs.google.com/document/d/1-HzBGh0iA7smXhLo0_N9tt55NvEGiwjke13oIg6swB4)

### :x: Close

- [ ] @ipedowitz - Update [this template](https://gitlab.com/gitlab-com/Product/-/blob/main/.gitlab/issue_templates/SaaS-Free-User-Async-Update.md) with the items noted in the `♻ Retro Thread` and link to any commit(s) or MR(s) made in response to the thread

/assign @ipedowitz
/confidential
