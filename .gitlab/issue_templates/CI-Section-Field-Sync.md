## 🎯 Intent

* Create a singular forum for the field team to provide input on product direction
* Share and learn between Product other teams

## 💡 Recording

<Add Link After Session> 

### 🤨 What should we discuss?

Field team members share the following:

* Customer Escalations 
* Highlight specific use cases to support
* Recent success & failure stories
* Developer Evangelism team updates and needs 
* Feedback on our direction/roadmap
* Other ways Product groups can help field teams be successful

Product team members share the following:

* Meaningful direction/roadmap updates
* Recent deliveries
* Highlight product indicatorsProduct metrics highlights
* Share interesting customer stories
* Ask for help on specific validation track activities

## ☑ Format

1. Collaborate asynchronously on this issue
1. Participating team members complete individual tasks
1. Sync meeting based on input on issue

### :o: Opening Tasks
* [ ] Set a due date to this issue as 1 business day prior to the scheduled sync meeting - @jreporter
* [ ] Add a retrospective thread to this issue - @jreporter

### :heavy_check_mark: Individual Tasks

| Team | Updates (Add URL to comment) |
| ------ | ------ |
| CSMs @rachel_fuerst @fsieverding |  |
| SAs   @cherryhan @kgoossens @cchampernowne @sbailey1 @DarwinJS |  |
| Developer Relations @iganbaruch |  | 
| Package @trizzi  | | 
| Pipeline Authoring @dhershkovitch  | | 
| Pipeline Execution @rutshah | | 
| Runner Core/Fleet @DarrenEastman  | | 
| Hosted Runners @gabrielengel_gl  | | 


### :x: Closing Tasks
* [ ] Add a reminder to team members to capture their followups in issues and add them to the description - @jreporter
* [ ] Make [adjustments to the template](https://gitlab.com/gitlab-com/Product/-/blob/main/.gitlab/issue_templates/CI-Section-Field-Sync.md) based on the retrospective thread -  @jreporter
* [ ] Share relevant outcomes and highlights in Slack -  @jreporter




/confidential

/assign @rachel_fuerst @cherryhan @DarwinJS @iganbaruch @trizzi @dhershkovitch @jreporter @gabrielengel_gl @rutshah 

/label ~"section::ci" ~"Field Sync" 


