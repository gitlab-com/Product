## Intent
Quarterly [CD Section](https://about.gitlab.com/direction/cd/) direction Review and AMA

## Everyone Can Contribute
- Please see the [Google Doc](https://docs.google.com/document/d/1w1jx8m6-rZhZYYfduuPpQNmaygHneSSUO99QW0waJ10/edit?usp=sharing), you can contribute synchronously or synchronously.

## Tasks
- [ ] Update contribution method in doc
- [ ] Schedule AMA/Review
- [ ] Communicate to team
- [ ] Followup tasks
- [ ] Followup on retro thread